<h1>[LAPORAN RESMI MODUL 3 KELOMPOK C06]</h1>

<h2>Daftar Isi</h2>

- [Soal 1](#soal-1) <br>
	- [Soal 1a](#soal-1a)
	- [Soal 1b](#soal-1b)
	- [Soal 1c](#soal-1c)
	- [Soal 1d](#soal-1d)
	- [Soal 1e](#soal-1e)
- [Soal 2](#soal-2) <br>
	- [Soal 2a](#soal-2a)
	- [Soal 2b](#soal-2b)
	- [Soal 2c](#soal-2c-dan-2d)
	- [Soal 2d](#soal-2e)
- [Soal 3](#soal-3) <br>
	- [Soal 3a](#soal-3a)
	- [Soal 3b](#soal-3b)
	- [Soal 3c](#soal-3c)
	- [Soal 3d](#soal-3d)
	- [Soal 3e](#soal-3e)
- [Kendala](#kendala)
	- [Kendala Soal 1](#kendala-soal-1-)
	- [Kendala Soal 2](#kendala-soal-2-)
	- [Kendala Soal 3](#kendala-soal-3-)

<h3>Soal 1</h3>
Novak adalah seorang mahasiswa biasa yang terlalu sering berselancar di internet. Pada suatu saat, Ia menemukan sebuah informasi bahwa ada suatu situs yang tidak memiliki pengguna. Ia mendownload berbagai informasi yang ada dari situs tersebut dan menemukan sebuah file dengan tulisan yang tidak jelas. Setelah diperhatikan lagi, kode tersebut berformat base64. Ia lalu meminta kepada anda untuk membuat program untuk memecahkan kode-kode di dalam file yang Ia simpan di drive dengan cara decoding dengan base 64. Agar lebih cepat, Ia sarankan untuk menggunakan thread.


<h4>Soal 1a</h4>
Download dua file zip dan unzip file zip tersebut di dua folder yang berbeda dengan nama quote untuk file zip quote.zip dan music untuk file zip music.zip. Unzip ini dilakukan dengan bersamaan menggunakan thread. <br>

Di dalam fungsi main / utama untuk mendownload file quote.zip dan music.zip dari google drive : 

```
if (child==0) {
		if(fork()==0){
			execl("/usr/bin/wget", "wget", "-O", "./quote.zip", "https://drive.google.com/uc?id=1jR67_JAyozZPs2oYtYqEZxeBLrs-k3dt&export=download", "-qq", NULL);
		}else{
			wait(0);
			execl("/usr/bin/wget", "wget", "-O", "./music.zip", "https://drive.google.com/uc?id=1_djk0z-cx8bgISFsMiUaXzty2cT4VZp1&export=download", "-qq", NULL);
		}
	}
	while ((wait(&status)) > 0);
	wait(0);
```

Untuk membuat thread : 

```
	while(i<3)
	{
		err=pthread_create(&(tid[i]),NULL,&soal_a,NULL);
		if(err!=0)
		{
			printf("\n can't create thread : [%s]",strerror(err));
		}
		else
		{
			printf("\n create thread success\n");
		}
		i++;
	}
	pthread_join(tid[0],NULL);
	pthread_join(tid[1],NULL);
```

yang selanjutnya mengarah ke fungsi soal_a. Pada fungsi soal_a, akan dilakukan pembuatan directory baru untuk masing-masing zip, yaitu folder quote dan folder music. Secara bersamaan dilakukan unzip kepada dua zip tadi dengan peletakkan file ke folder yang bersesuaian, kodenya adalah : 

```
void* soal_a(void *arg)
{
	char *argv1[] = {"clear", NULL};
	pthread_t id=pthread_self();

	if(pthread_equal(id,tid[0]))
	{
		child = fork();
		if (child==0) {
		    execv("/usr/bin/clear", argv1);
	    	}
	}
	else if(pthread_equal(id,tid[1]))
	{
	  child = fork();
	  if(child==0) {
		char *argv[] = {"mkdir", "-p", "/home/nadya/sisop/shift3/quote/",NULL};
		execv("/bin/mkdir", argv);
	  }
	  if(fork()==0) {
		char *argv1[] = {"mkdir", "-p", "/home/nadya/sisop/shift3/music/", NULL};
		execv("/bin/mkdir", argv1);

	  }
	fflush(stdout);
	sleep(1);
	}
	else if(pthread_equal(id,tid[2]))
	{

         child = fork();
            if (child==0) {
		char *argv[] = {"unzip", "/home/nadya/sisop/shift3/quote.zip", "-d", "/home/nadya/sisop/shift3/quote/", NULL};
		execv("/usr/bin/unzip", argv);
	    }
	    if (fork()==0){
		char *argv1[] = {"unzip", "/home/nadya/sisop/shift3/music.zip", "-d", "/home/nadya/sisop/shift3/music/", NULL};
		execv("/usr/bin/unzip", argv1);
	    }
	}

	return NULL;
}
```



<h4>Soal 1b</h4>
Decode semua file .txt yang ada dengan base 64 dan masukkan hasilnya dalam satu file .txt yang baru untuk masing-masing folder (Hasilnya nanti ada dua file .txt) pada saat yang sama dengan menggunakan thread dan dengan nama quote.txt dan music.txt. Masing-masing kalimat dipisahkan dengan newline/enter. <br>

Di dalam fungsi main / utama, akan dilakukan pemanggilan fungsi soal_b sebagai proses dalam pembuatan thread. Fungsi soal_b akan menangani proses pembuatan 2 file baru yakni quote.txt dan music.txt di masing-masing folder. Kodenya adalah sebagai berikut :

```
i=0;
	while(i<3)
	{
		err=pthread_create(&(tid1[i]),NULL,&soal_b,NULL);
		if(err!=0)
		{
			printf("\n can't create thread : [%s]",strerror(err));
		}
		else
		{
			printf("\n create thread success\n");
		}
		i++;
	}
	pthread_join(tid1[0],NULL);
	pthread_join(tid1[1],NULL);
	while ((wait(&status)) > 0);
```

Untuk fungsi soal_b nya akan nampak, seperti :

```
void* soal_b(void *arg)
{
	char *argv1[] = {"clear", NULL};
	pthread_t id=pthread_self();
	if(pthread_equal(id,tid1[0]))
	{
		child = fork();
		if (child==0) {
		    execv("/usr/bin/clear", argv1);
	    	}
	}
	else if(pthread_equal(id,tid1[1]))
	{
	  child = fork();
	  if(child==0) {
		chdir("/home/nadya/sisop/shift3/quote/");
		char *argv[] = {"usr/bin/touch", "quote.txt",NULL};
		execv("/usr/bin/touch", argv);
	  }
	  if(fork()==0) {
		chdir("/home/nadya/sisop/shift3/music/");
		char *argv[] = {"usr/bin/touch", "music.txt", NULL};
		execv("/usr/bin/touch", argv);

	  }
	fflush(stdout);
	sleep(1);
	}
	else if(pthread_equal(id,tid1[2]))
	{
	}

	return NULL;
}
```

Di dalam fungsi main juga terdapat proses pembuatan thread untuk pemanggilan base64 yang akan melangsungkan proses decoding dengan base64. Kodenya akan seperti :

```
	i=0;
	while(i<3)
	{
		err=pthread_create(&(tid2[i]),NULL,&base64,NULL);
		if(err!=0)
		{
			printf("\n can't create thread : [%s]",strerror(err));
		}
		else
		{
			printf("\n create thread success\n");
		}
		i++;
	}
	pthread_join(tid1[0],NULL);
	pthread_join(tid1[1],NULL);
	while ((wait(&status)) > 0);
```

Pemanggilan fungsi tersebut mengacu ke fungsi base64, kodenya adalah : 

```
void* base64(void *argv)
{
	child=fork();
	int status = 0;
	pthread_t id=pthread_self();
	if(child==0)
	{
		if(pthread_equal(id,tid2[0]))
		{
			child = fork();
			int status = 0;
			if(child==0)
			{
				char encoded_string[255], decoded_string[255], path[1000];
				struct dirent *dp;
				FILE *fp;
				FILE *fp1;
				fp1 = fopen("/home/nadya/sisop/shift3/music/music.txt", "w");

				DIR *dir = opendir("/home/nadya/sisop/shift3/music");

				if (!dir) return 0;

				while ((dp = readdir(dir)) != NULL)
				{
					if (strcmp(dp->d_name, ".") != 0 && strcmp(dp->d_name, "..") != 0 && strcmp(dp->d_name, "music.txt"))
					{
						strcpy(path, "/home/nadya/sisop/shift3/music/");
						strcat(path, dp->d_name);
						fp = fopen (path, "r");
						fscanf(fp, "%s", encoded_string);
						int len_str = strlen(encoded_string);
						fprintf(fp1, "%s\n", base64Decoder(encoded_string, len_str));
					}
				}
				fclose(fp);
				closedir(dir);
			}
		}
		else if(pthread_equal(id,tid2[1]))
		{
			child = fork();
			int status = 0;
			if(child==0)
			{
				char encoded_string[255], decoded_string[255], path[1000];
				struct dirent *dp;
				FILE *fp;
				FILE *fp1;
				fp1 = fopen("/home/nadya/sisop/shift3/quote/quote.txt", "w");

				DIR *dir = opendir("/home/nadya/sisop/shift3/quote");

				if (!dir) return 0;

				while ((dp = readdir(dir)) != NULL)
				{
					if (strcmp(dp->d_name, ".") != 0 && strcmp(dp->d_name, "..") != 0 && strcmp(dp->d_name, "quote.txt"))
					{
						strcpy(path, "/home/nadya/sisop/shift3/quote/");
						strcat(path, dp->d_name);
						fp = fopen (path, "r");
						fscanf(fp, "%s", encoded_string);
						int len_str = strlen(encoded_string);
						fprintf(fp1, "%s\n", base64Decoder(encoded_string, len_str));
					}
				}
				closedir(dir);
			}
		}
	}
}
```

Pada tahap tersebut akan dilakukan pemanggilan fungsi base64Decoder, fungsi tersebut adalah : 

```
char* base64Decoder(char encoded[], int len_str)
{
    char* decoded_string;
    decoded_string = (char*)malloc(sizeof(char) * SIZE);

    int i, j, k = 0;

    // stores the bitstream.
    int num = 0;

    // count_bits stores current
    // number of bits in num.
    int count_bits = 0;

    // selects 4 characters from
    // encoded string at a time.
    // find the position of each encoded
    // character in char_set and stores in num.
    for (i = 0; i < len_str; i += 4) {
        num = 0, count_bits = 0;
        for (j = 0; j < 4; j++) {
            // make space for 6 bits.
            if (encoded[i + j] != '=') {
                num = num << 6;
                count_bits += 6;
            }

            /* Finding the position of each encoded
            character in char_set
            and storing in "num", use OR
            '|' operator to store bits.*/

            // encoded[i + j] = 'E', 'E' - 'A' = 5
            // 'E' has 5th position in char_set.
            if (encoded[i + j] >= 'A' && encoded[i + j] <= 'Z')
                num = num | (encoded[i + j] - 'A');

            // encoded[i + j] = 'e', 'e' - 'a' = 5,
            // 5 + 26 = 31, 'e' has 31st position in char_set.
            else if (encoded[i + j] >= 'a' && encoded[i + j] <= 'z')
                num = num | (encoded[i + j] - 'a' + 26);

            // encoded[i + j] = '8', '8' - '0' = 8
            // 8 + 52 = 60, '8' has 60th position in char_set.
            else if (encoded[i + j] >= '0' && encoded[i + j] <= '9')
                num = num | (encoded[i + j] - '0' + 52);

            // '+' occurs in 62nd position in char_set.
            else if (encoded[i + j] == '+')
                num = num | 62;

            // '/' occurs in 63rd position in char_set.
            else if (encoded[i + j] == '/')
                num = num | 63;

            // ( str[i + j] == '=' ) remove 2 bits
            // to delete appended bits during encoding.
            else {
                num = num >> 2;
                count_bits -= 2;
            }
        }

        while (count_bits != 0) {
            count_bits -= 8;

            // 255 in binary is 11111111
            decoded_string[k++] = (num >> count_bits) & 255;
        }
    }

    // place NULL character to mark end of string.
    decoded_string[k] = '\0';

    return decoded_string;
}
```

Setelah itu, proses decoding telah berhasil disimpan ke dalam 2 file txt yang telah dibuat.

<h4>Soal 1c</h4>
Pindahkan kedua file .txt yang berisi hasil decoding ke folder yang baru bernama hasil.<br>

Untuk memindahkannya, akan dilakukan program : 

```
	if(fork()==0) {
		char *argv[] = {"mkdir", "-p", "/home/nadya/sisop/shift3/hasil/",NULL};
		execv("/bin/mkdir", argv);
	}
	if(fork()==0){
		if(fork()==0){
			char *argv[] = {"mv", "/home/nadya/sisop/shift3/quote/quote.txt", "/home/nadya/sisop/shift3/hasil", NULL};
			execv("/bin/mv", argv);
		}

		if(fork()==0){
			char *argv[] = {"mv", "/home/nadya/sisop/shift3/music/music.txt", "/home/nadya/sisop/shift3/hasil", NULL};
			execv("/bin/mv", argv);
		}
	}
	while ((wait(&status)) > 0);
```

<h4>Soal 1d</h4>
Folder hasil di-zip menjadi file hasil.zip dengan password 'mihinomenest[Nama user]'. (contoh password : mihinomenestnovak) <br>

Pada kasus ini, karena nama user yang didapat adalah "nadya", maka passwordnya ialah mihinomenestnadya. Untuk mendapatkan nama user, digunakan cara yang diperoleh dari modul 2. Kodenya adalah : 

```
	if(fork() == 0) {
		struct stat info;
		int r;
		r = stat("/home/nadya/sisop/shift3", &info);
		struct passwd *pw = getpwuid(info.st_uid);
		char password1[350] = "mihinomenest";
		if(pw != 0) strcat(password1, pw->pw_name);
		execl("/usr/bin/zip", "zip", "-r", "-e", "-P", password1, "/home/nadya/sisop/shift3/hasil", "hasil", "-q", NULL);
	}
```

<h4>Soal 1e</h4>
Karena ada yang kurang, kalian diminta untuk unzip file hasil.zip dan buat file no.txt dengan tulisan 'No' pada saat yang bersamaan, lalu zip kedua file hasil dan file no.txt menjadi hasil.zip, dengan password yang sama seperti sebelumnya. <br>

Karena diminta melakukan secara bersamaan, maka kita akan menggunakan thread. Maka dalam fungsi utama akan dituliskan kode : 

```
	i=0;
	while(i<3)
	{
		err=pthread_create(&(tid3[i]),NULL,&soal_e,NULL);
		if(err!=0)
		{
			printf("\n can't create thread : [%s]",strerror(err));
		}
		else
		{
			printf("\n create thread success\n");
		}
		i++;
	}
	pthread_join(tid3[0],NULL);
	pthread_join(tid3[1],NULL);
	pthread_join(tid3[2],NULL);
```

Karena kode sebelum ini mengacu ke fungsi soal_e. Soal_e akan melakukan unzip file hasil.zip dan membuat file txt baru yaitu no.txt yang di dalamnya terdapat kata "No". Kode dari fungsi tersebut ialah : 

```
void* soal_e(void *arg)
{
	char *argv1[] = {"clear", NULL};
	pthread_t id=pthread_self();
	if(pthread_equal(id,tid3[0]))
	{
		child = fork();
		if (child==0) {
		    execv("/usr/bin/clear", argv1);
	    	}
	}
	else if(pthread_equal(id,tid3[1]))
	{
	  child = fork();
	  if(child==0) {
		char *argv[] = {"unzip", "-P", "mihinomenestnadya", "hasil.zip", NULL};
		execv("/usr/bin/unzip", argv);
	  }
	fflush(stdout);
	sleep(1);
	}
	else if(pthread_equal(id,tid3[2]))
	{
	 child = fork();
	 if(child==0) {
		chdir("/home/nadya/sisop/shift3/");
		char *argv[] = {"usr/bin/touch", "no.txt", NULL};
		execv("/usr/bin/touch", argv);
	 }else{
		char *filename = "no.txt";
		FILE *fw = fopen(filename, "w");
		if (fw == NULL) {
			printf("Error\n");
			return 0;
		}
		fprintf(fw, "NO\n");
		fclose(fw);
	 }

	}

	return NULL;
}
```

Setelah proses tersebut berhasil, kini kita diminta mengulangi proses zip dengan menggabungkan folder hasil dan file no.txt. Caranya adalah : 

```
	if(fork() == 0) {
		struct stat info;
		int r;
		r = stat("/home/nadya/sisop/shift3", &info);
		struct passwd *pw = getpwuid(info.st_uid);
		char password1[350] = "mihinomenest";
		if(pw != 0) strcat(password1, pw->pw_name);
		execl("/usr/bin/zip", "zip", "-r", "-e", "-P", password1, "/home/nadya/sisop/shift3/hasil",  "./no.txt", "hasil", "-q", NULL);
	}
```

<br>
<strong>Dokumentasi : </strong>
<img src="foto/soal1/foto 1.png" alt="1.1">
<img src="foto/soal1/foto 2.png" alt="1.2">
<img src="foto/soal1/foto 3.png" alt="1.3">
<img src="foto/soal1/foto 4.png" alt="1.4">
<img src="foto/soal1/foto 5.png" alt="1.5">
<img src="foto/soal1/foto 6.png" alt="1.6">
<img src="foto/soal1/foto 7.png" alt="1.7">

<br>

Bluemary adalah seorang Top Global 1 di salah satu platform online judge. Suatu hari Ia ingin membuat online judge nya sendiri, namun dikarenakan Ia sibuk untuk mempertahankan top global nya, maka Ia meminta kamu untuk membantunya dalam membuat online judge
sederhana. Online judge sederhana akan dibuat dengan sistem client-server dengan beberapa kriteria sebagai berikut:

<h4>Soal 2a</h4>
Pada saat client terhubung ke server, terdapat dua pilihan pertama yaitu register dan login. Jika memilih register, client akan diminta input id dan passwordnya untuk dikirimkan keserver. Data input akan disimpan ke file users.txt dengan format username:password. Jika client memilih login, server juga akan meminta client untuk input id dan passwordnya lalu
server akan mencari data di users.txt yang sesuai dengan input client. Jika data yang sesuai ditemukan, maka client dapat login dan dapat menggunakan command-command yang ada pada sistem. Jika tidak maka server akan menolak login client. Username dan password
memiliki kriteria sebagai berikut:
● Username unique (tidak boleh ada user yang memiliki username yang sama)
● Password minimal terdiri dari 6 huruf, terdapat angka, terdapat huruf besar dan kecil<br>

Sebelum itu, terdapat komponen-komponen yang digunakan untuk menghubungkan server dengan client<br>
Server:

```
void error(char *err){
    perror(err);
    exit(1);
}
```

```
 if(argc != 2){
        fprintf(stderr, "ERROR: Port number not provided.");
        exit(1);
    }

    int socket_fd = socket(AF_INET, SOCK_STREAM, 0);
    if(socket_fd < 0) error("ERROR: Failed to open socket.\n");

    struct sockaddr_in server_addr;
    memset(&server_addr, '0', sizeof(server_addr));

    server_addr.sin_family = AF_INET;
    server_addr.sin_port = htons(PORT);

    if(inet_pton(AF_INET, "127.0.0.1", &server_addr.sin_addr)<=0) {
        printf("\nInvalid address/ Address not supported \n");
        return -1;
    }
    
    if(bind(socket_fd, (struct sockaddr *) &server_addr, sizeof(server_addr)) < 0) error("ERROR: Failed to bind socket.");
    listen(socket_fd, 5);
```

Client:
```
void error(char *err){
    perror(err);
    exit(1);
}
```

```
if(argc != 3){
        fprintf(stderr, "ERROR: Incorrect function parameter input.");
        exit(1);
    }

    int socket_fd = socket(AF_INET, SOCK_STREAM, 0);
    if(socket_fd < 0) error("ERROR: Failed to open socket.");

    struct hostent* server = gethostbyname(argv[1]);
    if(!server){
        fprintf(stderr, "ERROR: Server unavailable.");
    }
 
    struct sockaddr_in server_addr;
    memset(&server_addr, '0', sizeof(server_addr));

    server_addr.sin_family = AF_INET;
    server_addr.sin_port = htons(PORT);

    if(inet_pton(AF_INET, "127.0.0.1", &server_addr.sin_addr)<=0) {
        printf("\nInvalid address/ Address not supported \n");
        return -1;
    }

    if(connect(socket_fd, (struct sockaddr *) &server_addr, sizeof(server_addr)) < 0) error("ERROR: Failed to connect.");

    int buffer_len = 1024;
    char buffer_in[buffer_len];
    char buffer_out[buffer_len];
    int rw_flag;

    int logged_in = 0;

    printf("Initialized client on server %s with open port number %s\n", argv[1], argv[2]);
```

Terdapat pilihan register, login atau logout<br>

Server:

```
if(!mode){
                //WRITE PROMPT
                if(strlen(buffer_out) > 0) strcat(buffer_out, "\n1. login\n2. register\n3. exit\n\n");
                else strcpy(buffer_out, "1. login\n2. register\n3. exit\n\n");
                rw_flag = write(newsocket_fd, buffer_out, strlen(buffer_out));
                if(rw_flag < 0) error("ERROR: Failed to write to file");


                //READ INPUT
                bzero(buffer_in, buffer_len);
                rw_flag = read(newsocket_fd, buffer_in, buffer_len);
                if(rw_flag < 0) error("ERROR: Failed to read mode.");

                if(atoi(buffer_in) == 1){
                    mode = 1;
                }

                if(atoi(buffer_in) == 2){
                    mode = 2;               
                }

                if(atoi(buffer_in) == 3){
                    printf("\n-----END PROGRAM-----\n");
                    break;
                }
            } 
```

Client:

```
if(!mode){

            //READ PROMPT
            bzero(buffer_in, buffer_len);
            rw_flag = read(socket_fd, buffer_in, buffer_len);
            if(rw_flag < 0) error("ERROR: Failed to read to file.");
            printf("%s", buffer_in);

            //WRITE INPUT
            bzero(buffer_out, buffer_len);
            fgets(buffer_out, buffer_len, stdin);
            rw_flag = write(socket_fd, buffer_out, strlen(buffer_out));
            if(rw_flag < 0) error("ERROR: Failed to read file.");
            printf("\n");
            
            if(atoi(buffer_out) == 3){
                printf("-----END PROGRAM-----\n");
                break;
            }
        }
```
Setelah memasukkan pilihan "1" untuk login atau "2" untuk register atau "3" untuk logout. Program akan dijalankan sesuai input tersebut.<br>
Untuk Login <br>
Server:
```
else if(mode == 1){
                valid_user = false;
                valid_pass = false; 
                
                bzero(buffer_in, buffer_len);
                bzero(buffer_out, buffer_len);

                bzero(user, buffer_len);
                bzero(pass, buffer_len);
                bzero(copy, buffer_len);

                ptr = NULL;
                index = 0;

                submode = 0;
                //WRITE MODE
                if(1){
                    bzero(buffer_out, buffer_len);
                    sprintf(buffer_out, "%d %d", mode, submode);
                    rw_flag = write(newsocket_fd, buffer_out, buffer_len);
                    if(rw_flag < 0) error("ERROR: Failed to write to file");
                    bzero(buffer_out, buffer_len);

                }

                strcpy(buffer_out, "");

                while(!valid_user){
                    //WRITE PROMPT
                    strcat(buffer_out, "ENTER USERNAME: ");
                    rw_flag = write(newsocket_fd, buffer_out, strlen(buffer_out));
                    if(rw_flag < 0) error("ERROR: Failed to write to file");


                    //READ INPUT
                    bzero(buffer_in, buffer_len);
                    rw_flag = read(newsocket_fd, buffer_in, buffer_len);
                    if(rw_flag < 0) error("ERROR: Failed to read username.");

                    valid_user = check_login(buffer_in);
                    
                    if(!valid_user) {
                        bzero(buffer_out, buffer_len);
                        strcpy(buffer_out, "ERROR: USERNAME DOES NOT EXIST. TRY AGAIN\n");
                    }else{
                        strcpy(user, buffer_in);
                        submode = 1;
                    }
                    
                    //WRITE MODE
                    if(1){
                        bzero(buffer_out, buffer_len);
                        sprintf(buffer_out, "%d %d", mode, submode);
                        rw_flag = write(newsocket_fd, buffer_out, buffer_len);
                        if(rw_flag < 0) error("ERROR: Failed to write to file");
                        bzero(buffer_out, buffer_len);

                    }
                }
                
                bzero(buffer_out, buffer_len);

                while(!valid_pass){
                    if(!mode) break;

                    //WRITE PROMPT
                    strcat(buffer_out, "ENTER PASSWORD: ");
                    rw_flag = write(newsocket_fd, buffer_out, strlen(buffer_out));
                    if(rw_flag < 0) error("ERROR: Failed to write to file");

                    //READ INPUT
                    bzero(buffer_in, buffer_len);
                    rw_flag = read(newsocket_fd, buffer_in, buffer_len);
                    if(rw_flag < 0) error("ERROR: Failed to read password.\n");

                    valid_pass = login_pass(user, buffer_in);

                    if(!valid_pass) {
                        bzero(buffer_out, buffer_len);
                        strcpy(buffer_out, "ERROR: INCORRECT PASSWORD.\n");
                        mode = 0;
                        submode = 0;
                    }else{
                        mode = 3;
                        submode = 0;
                    }
                    //WRITE MODE
                    if(1){
                        bzero(buffer_out, buffer_len);
                        sprintf(buffer_out, "%d %d", mode, submode);
                        rw_flag = write(newsocket_fd, buffer_out, buffer_len);
                        if(rw_flag < 0) error("ERROR: Failed to write to file");
                        bzero(buffer_out, buffer_len);

                    }
                }
```

Diikuti fungsi check_login untuk mencari username

```
bool check_login(char* buffer){
    FILE *fp = fopen("/home/aaliyah/sisop/modul3/users.txt", "r");
    int len =1024;
    char temp[len];
    char copy[len];

    char* ptr;
    long long index;

    ptr = strchr(buffer, '\n');

    if(ptr != NULL){
        index = (long long) (ptr - buffer);
        strcpy(copy, buffer);
        bzero(buffer, len);
        strncpy(buffer, copy, index);
    }

    while(fgets(temp, len, fp)) {
        if(temp == NULL) break;

        ptr = strchr(temp, ':');
        if(ptr != NULL){
            index = (int) (ptr - temp);
            strcpy(copy, temp);
            bzero(temp, len);
            strncpy(temp, copy, index);
        }

        printf("TEMP IS ------------%s-----------", temp);
        printf("%s strcmp %s == %d\n\n\n\n\n", buffer, temp, strcmp(buffer, temp));
        if(!strcmp(buffer, temp)) {
            fclose(fp);
            return true;
        }
    }

    fclose(fp);

    return false;
}
```

Diikuti fungsi login_pass untuk mencari password

```
bool login_pass(char* user, char* buffer){
    FILE *fp = fopen("/home/aaliyah/sisop/modul3/users.txt", "r");
    int len = 1024;
    char temp[len];
    char copy[len];

    char* ptr;
    long long index;
    
    ptr = strchr(buffer, '\n');
    if(ptr != NULL){
        index = (long long) (ptr - buffer);
        strcpy(copy, buffer);
        bzero(buffer, len);
        strncpy(buffer, copy, index);
    }

    while(fgets(temp, len, fp)) {
        if(temp == NULL) break;

        char temp_user[len];
        char* token = strtok(temp, ":");
        strcpy(temp_user, token);

        if(!strcmp(temp_user, user)) {
            // printf("FOUND USERNAME\n");
            fclose(fp);
            token = strtok(NULL, ":");
            if(token != NULL){
                // printf("TOKEN IS %s\n", token);
                remove_char(token, strlen(token) - 1);
                return !strcmp(token, buffer);
            }
        }
    }

    fclose(fp);

    return false;
}
```

Untuk Register:

```
else if(mode == 2){
                valid_user = false;
                valid_pass = false; 
                
                bzero(buffer_in, buffer_len);
                bzero(buffer_out, buffer_len);

                bzero(user, buffer_len);
                bzero(pass, buffer_len);
                bzero(copy, buffer_len);

                ptr = NULL;
                index = 0;
                
                submode = 0;
                //WRITE MODE
                if(1){
                    bzero(buffer_out, buffer_len);
                    sprintf(buffer_out, "%d %d", mode, submode);
                    rw_flag = write(newsocket_fd, buffer_out, buffer_len);
                    if(rw_flag < 0) error("ERROR: Failed to write to file");
                    bzero(buffer_out, buffer_len);

                }

                strcpy(buffer_out, "");

                while(!valid_user){
                    strcat(buffer_out, "REGISTER USERNAME: ");
                    rw_flag = write(newsocket_fd, buffer_out, strlen(buffer_out));
                    if(rw_flag < 0) error("ERROR: Failed to write to file");


                    bzero(buffer_in, buffer_len);
                    rw_flag = read(newsocket_fd, buffer_in, buffer_len);
                    if(rw_flag < 0) error("ERROR: Failed to read username.");

                    valid_user = check_user(buffer_in);
                    
                    if(!valid_user) {
                        bzero(buffer_out, buffer_len);
                        strcpy(buffer_out, "ERROR: USERNAME ALREADY TAKEN. TRY ANOTHER\n");
                    }else{
                        strcpy(user, buffer_in);
                    }
                    
                    if(valid_user) submode = 1;
                    //WRITE MODE
                    if(1){
                        bzero(buffer_out, buffer_len);
                        sprintf(buffer_out, "%d %d", mode, submode);
                        rw_flag = write(newsocket_fd, buffer_out, buffer_len);
                        if(rw_flag < 0) error("ERROR: Failed to write to file");
                        bzero(buffer_out, buffer_len);

                    }
                }
                

                while(!valid_pass){
                    strcpy(buffer_out, "REGISTER PASSWORD: ");
                    rw_flag = write(newsocket_fd, buffer_out, strlen(buffer_out));
                    if(rw_flag < 0) error("ERROR: Failed to write to file");

                    //READ INPUT
                    bzero(buffer_in, buffer_len);
                    rw_flag = read(newsocket_fd, buffer_in, buffer_len);
                    if(rw_flag < 0) error("ERROR: Failed to read password.");

                    valid_pass = check_pass(buffer_in);

                    if(!valid_pass) {
                        bzero(buffer_out, buffer_len);
                        strcpy(buffer_out, "ERROR: INVALID PASSWORD FORMAT. MUST HAVE 6 ALPHABET, CAPITAL AND LOWER CASE, AND A NUMBER.\n");
                    }else{
                        strcpy(pass, buffer_in);
                    }
                    
                    if(valid_pass) submode = 2;
                    //WRITE MODE
                    if(1){
                        bzero(buffer_out, buffer_len);
                        sprintf(buffer_out, "%d %d", mode, submode);
                        rw_flag = write(newsocket_fd, buffer_out, buffer_len);
                        if(rw_flag < 0) error("ERROR: Failed to write to file");
                        bzero(buffer_out, buffer_len);

                    }
                }
                

                bzero(buffer_out, buffer_len);
                strcpy(buffer_out, strcat(strcat(user, ":"), pass));
                
                ptr = strchr(buffer_out, '\n');
                if(ptr != NULL){
                    index = (int) (ptr - buffer_out);
                    strcpy(copy, buffer_out);
                    bzero(buffer_out, buffer_len);
                    strncpy(buffer_out, copy, index);
                }    

                FILE *fp = fopen("/home/aaliyah/sisop/modul3/users.txt", "a");
                if(fp == NULL) error("ERROR: Failed to open users.txt file.");
                fprintf(fp, "%s\n", buffer_out);

                fclose(fp);

                mode = 0;
                submode = 0;
            }
```

<h4>Soal 2b</h4>
File tsv tidak dibuat fungsi secara khusus karena setiap penambahan data akan membuka file problems.tsv dengan format "a+" yang mana apabila file tersebut tidak ada, maka akan dibuat file baru. Apabila ada akan menulis pada file tersebut

```
 FILE *database_file;
                    database_file = fopen("/home/aaliyah/sisop/modul3/problems.tsv", "a+");
                    if(database_file == NULL) {
						error("ERROR: FAILED TO OPEN DATABASE.\n");
                    }

                    fprintf(database_file, "%s", buffer_out);

                    fclose(database_file);
```

<h4>Soal 2c</h4>


<h4>Soal 2d</h4>


<h4>Soal 2e</h4>


<br>

<h3>Soal 3</h3>
Nami adalah seorang pengoleksi harta karun handal. Karena Nami memiliki waktu luang, Nami pun mencoba merapikan harta karun yang dimilikinya berdasarkan jenis/tipe/kategori/ekstensi harta karunnya. Setelah harta karunnya berhasil dikategorikan, Nami pun mengirimkan harta karun tersebut ke kampung halamannya

<h4>Soal 3a</h4>
Hal pertama yang perlu dilakukan oleh Nami adalah mengextract zip yang diberikan ke dalam folder “/home/[user]/shift3/”. Kemudian working directory program akan berada pada folder “/home/[user]/shift3/hartakarun/”. Karena Nami tidak ingin ada file yang tertinggal, program harus mengkategorikan seluruh file pada working directory secara rekursif


```
void takerec(char *beloksini){
    char path[PATH_MAX];
    struct dirent *dp;
    struct stat buffer;
    DIR *dir = opendir(beloksini);
    if(!dir) return;
    while((dp = readdir(dir)) != NULL){
        if(strcmp(dp->d_name, ".") && strcmp(dp->d_name, "..")){
            char buffer1[200];
            int buffer_len1 = sizeof(buffer1);
            snprintf(buffer1, buffer_len1, "%s/%s", beloksini, dp->d_name);
            strcpy(path, buffer1);
            if(!stat(path, &buffer) && S_ISREG(buffer.st_mode)){
                pthread_t thread;
                int err = pthread_create(&thread, NULL, exc, (void *)path);
                pthread_join(thread, NULL);
            }
            takerec(path);
        }
    } closedir(dir);
}
```
<img src="foto/soal3/3a.png" alt="3a">

<h4>Soal 3b</h4>
Semua file harus berada di dalam folder, jika terdapat file yang tidak memiliki ekstensi, file disimpan dalam folder “Unknown”. Jika file hidden, masuk folder “Hidden”.


```
if(hidden[1] == '.'){
        strcpy(dirname, "hartakarun/");
        strcat(dirname, "Hidden");
    }else if (strstr(fn, ".") != NULL){
        // printf("fn : %s\n", fn);
        strcpy(file, fn);
        strtok(file, ".");
        char *token = strtok(NULL, "");
        int i;
        for (i = 0; token[i]; i++){
            token[i] = tolower(token[i]);
            // if(i > 1) break;
        }
        // printf("token : %s\n", token);
        char bener[200], realtampung[200];
        strcpy(bener, "");
        strcpy(realtampung, "");
        int a = strlen(exist_f) - 1;
        int ok = 0, idb = 0;
        // printf("gede : %d %c\n", a, exist_f[a]);
        while(a >= 0){
            // printf("cmonnn : %d\n", exist_f[a] == 'g');
            if(exist_f[a] == '.'){
                // puts("yalord");
                if(!ok){
                    // printf("yeyyy\n");
                    ok = 1;
                    strcpy(realtampung, bener);
                }else{
                    // printf("yopie\n");
                    strcpy(realtampung, bener);
                    break;
                }
            }else if(exist_f[a] == '/') break;
            char buffer1[200];
            int buffer_len1 = sizeof(buffer1);
            snprintf(buffer1, buffer_len1, "%s%c", bener, tolower(exist_f[a]));
            strcpy(bener, buffer1);
            a--;
        }
        // printf("ini bener : %s\n", realtampung);
        int lastt = strlen(realtampung) - 1;
        char answer_path[200] = "";
        for(; lastt >= 0; lastt--){
            char buffer1[200];
            int buffer_len1 = sizeof(buffer1);
            snprintf(buffer1, buffer_len1, "%c", realtampung[lastt]);
            strcat(answer_path, buffer1);
        }//printf("%s\n", answer_path);
        strcpy(dirname, "hartakarun/");
        strcat(dirname, answer_path);
        // strcat(dirname, token);
    }else{
        strcpy(dirname, "hartakarun/");
        strcat(dirname, "Unknown");
    }
```

<img src="foto/soal3/3b1.png" alt="3b1">
<img src="foto/soal3/3b2.png" alt="3b2">
<img src="foto/soal3/3b3.png" alt="3b3">

<h4>Soal 3c</h4>
Agar proses kategori bisa berjalan lebih cepat, setiap 1 file yang dikategorikan dioperasikan oleh 1 thread.


```
while((dp = readdir(dir)) != NULL){
        if(strcmp(dp->d_name, ".") && strcmp(dp->d_name, "..")){
            char buffer1[200];
            int buffer_len1 = sizeof(buffer1);
            snprintf(buffer1, buffer_len1, "%s/%s", beloksini, dp->d_name);
            strcpy(path, buffer1);
            if(!stat(path, &buffer) && S_ISREG(buffer.st_mode)){
                pthread_t thread;
                int err = pthread_create(&thread, NULL, exc, (void *)path);
                pthread_join(thread, NULL);
            }
            takerec(path);
        }
    } closedir(dir);
```

<h4>Soal 3d</h4>
Untuk mengirimkan file ke Cocoyasi Village, nami menggunakan program client-server. Saat program client dijalankan, maka folder /home/[user]/shift3/hartakarun/” akan di-zip terlebih dahulu dengan nama “hartakarun.zip” ke working directory dari program client.

```
if (connect(sock, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0) {
        printf("\nConnection Failed \n");
        return -1;
    }
    pid_t weap_ch;
    weap_ch = fork();
    if(weap_ch < 0){
        exit(EXIT_FAILURE);
    }
    if(!weap_ch) execl("/usr/bin/zip", "zip", "-r", "hartakarun", "../hartakarun", "-q", NULL);
    send(sock , hello , strlen(hello) , 0 );
```

<img src="foto/soal3/3d.png" alt="3d">

<h4>Soal 3e</h4>
Client dapat mengirimkan file “hartakarun.zip” ke server dengan mengirimkan command berikut ke server


```
if(!strcmp(buffer, "send hartakarun.zip")){
        char cwd[PATH_MAX];
        getcwd(cwd, sizeof(cwd));
        int apaini = 0;
        while(cwd[apaini] != '\0') apaini++;
        // printf("%d\n", apaini);
        char ini_path[200];
        strncpy (ini_path, cwd, apaini - 7);
        char goalpath[200], startpath[200];
        strcpy(startpath, ini_path);
        strcpy(goalpath, ini_path);
        strcat(startpath, "/Client/hartakarun.zip");
        strcat(goalpath, "/Server/hartakarun.zip");
        rename(startpath, goalpath);
        // printf("%s\n", ini_path);
    }
```

<img src="foto/soal3/3e.png" alt="3e">
<img src="foto/soal3/3e1.png" alt="3e1">

<h2>Kendala</h2>
Dalam pengerjaan soal-soal di atas ditemui beberapa kendala yang akan dijabarkan berdasarkan soal. Berikut kendala-kendala yang ditemui:<br>
<h5>Kendala Soal 1 : </h5>
<ol type="i">
    <li>Di awal terdapat error ketika download quote.zip dan music.zip</li>
	<li>Kesulitan dalam decode base64 karena di awal tidak menemukan referensi</li>
	<li>Kesulitan ketika zip hasil dan no.txt</li>
</ol>

<h5>Kendala Soal 2 : </h5>
<ol type="i">
	<li>Kesulitan saat error di segmentation fault</li>
	<li>dua</li>
    <li>tiga</li>
    <li>empat</li>
</ol>

<h5>Kendala Soal 3 : </h5>
<ol type="i">
	<li>Terlalu banyak string handling sehingga kadang rancu</li>
</ol>
