#include<ctype.h>
#include<dirent.h>
#include<string.h>
#include<unistd.h>
#include<sys/stat.h>
#include<pthread.h>
#include<stdio.h>
#include<sys/types.h>

// char *remove(char* myStr) {
//     char *retStr;
//     char *lastExt;
//     if (myStr == NULL) return NULL;
//     if ((retStr = malloc (strlen (myStr) + 1)) == NULL) return NULL;
//     strcpy (retStr, myStr);
//     lastExt = strrchr (retStr, '.');
//     if (lastExt != NULL)
//         *lastExt = '\0';
//     return retStr;
// }

int cek(const char *fn){
    struct stat banding;
    if(!stat(fn, &banding)) return 1;
    return 0;
}


void *exc(void *fn){
    char exist_f[200], dirname[200], cwd[PATH_MAX], hidden_f[200], file[200], hidden[200];
    // printf("parameter filename : %s\n", filename);
    strcpy(exist_f, fn);
    strcpy(hidden_f, fn);
    char *yaini = strrchr(hidden_f, '/');
    strcpy(hidden, yaini);
    if(hidden[1] == '.'){
        strcpy(dirname, "hartakarun/");
        strcat(dirname, "Hidden");
    }else if (strstr(fn, ".") != NULL){
        // printf("fn : %s\n", fn);
        strcpy(file, fn);
        strtok(file, ".");
        char *token = strtok(NULL, "");
        int i;
        for (i = 0; token[i]; i++){
            token[i] = tolower(token[i]);
            // if(i > 1) break;
        }
        // printf("token : %s\n", token);
        char bener[200], realtampung[200];
        strcpy(bener, "");
        strcpy(realtampung, "");
        int a = strlen(exist_f) - 1;
        int ok = 0, idb = 0;
        // printf("gede : %d %c\n", a, exist_f[a]);
        while(a >= 0){
            // printf("cmonnn : %d\n", exist_f[a] == 'g');
            if(exist_f[a] == '.'){
                // puts("yalord");
                if(!ok){
                    // printf("yeyyy\n");
                    ok = 1;
                    strcpy(realtampung, bener);
                }else{
                    // printf("yopie\n");
                    strcpy(realtampung, bener);
                    break;
                }
            }else if(exist_f[a] == '/') break;
            char buffer1[200];
            int buffer_len1 = sizeof(buffer1);
            snprintf(buffer1, buffer_len1, "%s%c", bener, tolower(exist_f[a]));
            strcpy(bener, buffer1);
            a--;
        }
        // printf("ini bener : %s\n", realtampung);
        int lastt = strlen(realtampung) - 1;
        char answer_path[200] = "";
        for(; lastt >= 0; lastt--){
            char buffer1[200];
            int buffer_len1 = sizeof(buffer1);
            snprintf(buffer1, buffer_len1, "%c", realtampung[lastt]);
            strcat(answer_path, buffer1);
        }//printf("%s\n", answer_path);
        strcpy(dirname, "hartakarun/");
        strcat(dirname, answer_path);
        // strcat(dirname, token);
    }else{
        strcpy(dirname, "hartakarun/");
        strcat(dirname, "Unknown");
    }
    // printf("EXISTFILE : %s\n", exist_f);
    int exist = cek(exist_f);
    // printf("dirname : %s\n", dirname);
    if(!strcmp(dirname, "hartakarun/_")) strcpy(dirname, "hartakarun/__");
    if(exist){
        mkdir(dirname, 0777);
        // if(mkdir(dirname, 0777) == -1)
            // puts("File sudah ada!");
        // puts("ok!");
    }else puts("not okay!");
    if(getcwd(cwd, sizeof(cwd)) != NULL){
        char *nama = strrchr(fn, '/');
        char namafile[200];
        strcpy(namafile, cwd);
        strcat(namafile, "/");
        strcat(namafile, dirname);
        strcat(namafile, nama);
        // printf("file = %s\nnama = %s\n", fn, namafile);
        rename(fn, namafile);
    }
}

void takerec(char *beloksini){
    char path[PATH_MAX];
    struct dirent *dp;
    struct stat buffer;
    DIR *dir = opendir(beloksini);
    if(!dir) return;
    while((dp = readdir(dir)) != NULL){
        if(strcmp(dp->d_name, ".") && strcmp(dp->d_name, "..")){
            char buffer1[200];
            int buffer_len1 = sizeof(buffer1);
            snprintf(buffer1, buffer_len1, "%s/%s", beloksini, dp->d_name);
            strcpy(path, buffer1);
            if(!stat(path, &buffer) && S_ISREG(buffer.st_mode)){
                pthread_t thread;
                int err = pthread_create(&thread, NULL, exc, (void *)path);
                pthread_join(thread, NULL);
            }
            takerec(path);
        }
    } closedir(dir);
}

int main(int argc, char *argv[]){
    char cwd[PATH_MAX];
    if(getcwd(cwd, sizeof(cwd)) != NULL){
        strcat(cwd, "/hartakarun");
        // printf("%s\n", cwd);
        takerec(cwd);
    }
}

//gcc -pthread -o soal3 soal3.c