#include<stdio.h>
#include<string.h>
#include<pthread.h>
#include<stdlib.h>
#include<unistd.h>
#include<sys/types.h>
#include<sys/wait.h>
#include<sys/stat.h>
#include<fcntl.h>
#include<stdbool.h>
#include<errno.h>
#include<syslog.h>
#include<dirent.h>
#include<pwd.h>
#define SIZE 100

pthread_t tid[3]; //inisialisasi array untuk menampung thread dalam kasus ini ada 2 thread
pthread_t tid1[3];
pthread_t tid2[3];
pthread_t tid3[3];
pid_t child;
int status;

char* base64Decoder(char encoded[], int len_str)
{
    char* decoded_string;
    decoded_string = (char*)malloc(sizeof(char) * SIZE);

    int i, j, k = 0;

    // stores the bitstream.
    int num = 0;

    // count_bits stores current
    // number of bits in num.
    int count_bits = 0;

    // selects 4 characters from
    // encoded string at a time.
    // find the position of each encoded
    // character in char_set and stores in num.
    for (i = 0; i < len_str; i += 4) {
        num = 0, count_bits = 0;
        for (j = 0; j < 4; j++) {
            // make space for 6 bits.
            if (encoded[i + j] != '=') {
                num = num << 6;
                count_bits += 6;
            }

            /* Finding the position of each encoded
            character in char_set
            and storing in "num", use OR
            '|' operator to store bits.*/

            // encoded[i + j] = 'E', 'E' - 'A' = 5
            // 'E' has 5th position in char_set.
            if (encoded[i + j] >= 'A' && encoded[i + j] <= 'Z')
                num = num | (encoded[i + j] - 'A');

            // encoded[i + j] = 'e', 'e' - 'a' = 5,
            // 5 + 26 = 31, 'e' has 31st position in char_set.
            else if (encoded[i + j] >= 'a' && encoded[i + j] <= 'z')
                num = num | (encoded[i + j] - 'a' + 26);

            // encoded[i + j] = '8', '8' - '0' = 8
            // 8 + 52 = 60, '8' has 60th position in char_set.
            else if (encoded[i + j] >= '0' && encoded[i + j] <= '9')
                num = num | (encoded[i + j] - '0' + 52);

            // '+' occurs in 62nd position in char_set.
            else if (encoded[i + j] == '+')
                num = num | 62;

            // '/' occurs in 63rd position in char_set.
            else if (encoded[i + j] == '/')
                num = num | 63;

            // ( str[i + j] == '=' ) remove 2 bits
            // to delete appended bits during encoding.
            else {
                num = num >> 2;
                count_bits -= 2;
            }
        }

        while (count_bits != 0) {
            count_bits -= 8;

            // 255 in binary is 11111111
            decoded_string[k++] = (num >> count_bits) & 255;
        }
    }

    // place NULL character to mark end of string.
    decoded_string[k] = '\0';

    return decoded_string;
}


void* base64(void *argv)
{
	child=fork();
	int status = 0;
	pthread_t id=pthread_self();
	if(child==0)
	{
		if(pthread_equal(id,tid2[0]))
		{
			child = fork();
			int status = 0;
			if(child==0)
			{
				char encoded_string[255], decoded_string[255], path[1000];
				struct dirent *dp;
				FILE *fp;
				FILE *fp1;
				fp1 = fopen("/home/nadya/sisop/shift3/music/music.txt", "w");

				DIR *dir = opendir("/home/nadya/sisop/shift3/music");

				if (!dir) return 0;

				while ((dp = readdir(dir)) != NULL)
				{
					if (strcmp(dp->d_name, ".") != 0 && strcmp(dp->d_name, "..") != 0 && strcmp(dp->d_name, "music.txt"))
					{
						strcpy(path, "/home/nadya/sisop/shift3/music/");
						strcat(path, dp->d_name);
						fp = fopen (path, "r");
						fscanf(fp, "%s", encoded_string);
						int len_str = strlen(encoded_string);
						fprintf(fp1, "%s\n", base64Decoder(encoded_string, len_str));
					}
				}
				fclose(fp);
				closedir(dir);
			}
		}
		else if(pthread_equal(id,tid2[1]))
		{
			child = fork();
			int status = 0;
			if(child==0)
			{
				char encoded_string[255], decoded_string[255], path[1000];
				struct dirent *dp;
				FILE *fp;
				FILE *fp1;
				fp1 = fopen("/home/nadya/sisop/shift3/quote/quote.txt", "w");

				DIR *dir = opendir("/home/nadya/sisop/shift3/quote");

				if (!dir) return 0;

				while ((dp = readdir(dir)) != NULL)
				{
					if (strcmp(dp->d_name, ".") != 0 && strcmp(dp->d_name, "..") != 0 && strcmp(dp->d_name, "quote.txt"))
					{
						strcpy(path, "/home/nadya/sisop/shift3/quote/");
						strcat(path, dp->d_name);
						fp = fopen (path, "r");
						fscanf(fp, "%s", encoded_string);
						int len_str = strlen(encoded_string);
						fprintf(fp1, "%s\n", base64Decoder(encoded_string, len_str));
					}
				}
				closedir(dir);
			}
		}
	}
}


void* soal_a(void *arg)
{
	char *argv1[] = {"clear", NULL};
	pthread_t id=pthread_self();

	if(pthread_equal(id,tid[0]))
	{
		child = fork();
		if (child==0) {
		    execv("/usr/bin/clear", argv1);
	    	}
	}
	else if(pthread_equal(id,tid[1]))
	{
	  child = fork();
	  if(child==0) {
		char *argv[] = {"mkdir", "-p", "/home/nadya/sisop/shift3/quote/",NULL};
		execv("/bin/mkdir", argv);
	  }
	  if(fork()==0) {
		char *argv1[] = {"mkdir", "-p", "/home/nadya/sisop/shift3/music/", NULL};
		execv("/bin/mkdir", argv1);

	  }
	fflush(stdout);
	sleep(1);
	}
	else if(pthread_equal(id,tid[2]))
	{

         child = fork();
            if (child==0) {
		char *argv[] = {"unzip", "/home/nadya/sisop/shift3/quote.zip", "-d", "/home/nadya/sisop/shift3/quote/", NULL};
		execv("/usr/bin/unzip", argv);
	    }
	    if (fork()==0){
		char *argv1[] = {"unzip", "/home/nadya/sisop/shift3/music.zip", "-d", "/home/nadya/sisop/shift3/music/", NULL};
		execv("/usr/bin/unzip", argv1);
	    }
	}

	return NULL;
}


void* soal_b(void *arg)
{
	char *argv1[] = {"clear", NULL};
	pthread_t id=pthread_self();
	if(pthread_equal(id,tid1[0]))
	{
		child = fork();
		if (child==0) {
		    execv("/usr/bin/clear", argv1);
	    	}
	}
	else if(pthread_equal(id,tid1[1]))
	{
	  child = fork();
	  if(child==0) {
		chdir("/home/nadya/sisop/shift3/quote/");
		char *argv[] = {"usr/bin/touch", "quote.txt",NULL};
		execv("/usr/bin/touch", argv);
	  }
	  if(fork()==0) {
		chdir("/home/nadya/sisop/shift3/music/");
		char *argv[] = {"usr/bin/touch", "music.txt", NULL};
		execv("/usr/bin/touch", argv);

	  }
	fflush(stdout);
	sleep(1);
	}
	else if(pthread_equal(id,tid1[2]))
	{
	}

	return NULL;
}

void* soal_e(void *arg)
{
	char *argv1[] = {"clear", NULL};
	pthread_t id=pthread_self();
	if(pthread_equal(id,tid3[0]))
	{
		child = fork();
		if (child==0) {
		    execv("/usr/bin/clear", argv1);
	    	}
	}
	else if(pthread_equal(id,tid3[1]))
	{
	  child = fork();
	  if(child==0) {
		char *argv[] = {"unzip", "-P", "mihinomenestnadya", "hasil.zip", NULL};
		execv("/usr/bin/unzip", argv);
	  }
	fflush(stdout);
	sleep(1);
	}
	else if(pthread_equal(id,tid3[2]))
	{
	 child = fork();
	 if(child==0) {
		chdir("/home/nadya/sisop/shift3/");
		char *argv[] = {"usr/bin/touch", "no.txt", NULL};
		execv("/usr/bin/touch", argv);
	 }else{
		char *filename = "no.txt";
		FILE *fw = fopen(filename, "w");
		if (fw == NULL) {
			printf("Error\n");
			return 0;
		}
		fprintf(fw, "NO\n");
		fclose(fw);
	 }

	}

	return NULL;
}


int main(void)
{
	int i=0;
	int err;
	int status1;
	 child = fork();
	if (child==0) {
		if(fork()==0){
			execl("/usr/bin/wget", "wget", "-O", "./quote.zip", "https://drive.google.com/uc?id=1jR67_JAyozZPs2oYtYqEZxeBLrs-k3dt&export=download", "-qq", NULL);
		}else{
			wait(0);
			execl("/usr/bin/wget", "wget", "-O", "./music.zip", "https://drive.google.com/uc?id=1_djk0z-cx8bgISFsMiUaXzty2cT4VZp1&export=download", "-qq", NULL);
		}
	}
	while ((wait(&status)) > 0);
	wait(0);
	while(i<3)
	{
		err=pthread_create(&(tid[i]),NULL,&soal_a,NULL);
		if(err!=0)
		{
			printf("\n can't create thread : [%s]",strerror(err));
		}
		else
		{
			printf("\n create thread success\n");
		}
		i++;
	}
	pthread_join(tid[0],NULL);
	pthread_join(tid[1],NULL);

	while ((wait(&status)) > 0);
	i=0;
	while(i<3)
	{
		err=pthread_create(&(tid1[i]),NULL,&soal_b,NULL);
		if(err!=0)
		{
			printf("\n can't create thread : [%s]",strerror(err));
		}
		else
		{
			printf("\n create thread success\n");
		}
		i++;
	}
	pthread_join(tid1[0],NULL);
	pthread_join(tid1[1],NULL);
	while ((wait(&status)) > 0);

	i=0;
	while(i<3)
	{
		err=pthread_create(&(tid2[i]),NULL,&base64,NULL);
		if(err!=0)
		{
			printf("\n can't create thread : [%s]",strerror(err));
		}
		else
		{
			printf("\n create thread success\n");
		}
		i++;
	}
	pthread_join(tid1[0],NULL);
	pthread_join(tid1[1],NULL);
	while ((wait(&status)) > 0);


	//soal c
	if(fork()==0) {
		char *argv[] = {"mkdir", "-p", "/home/nadya/sisop/shift3/hasil/",NULL};
		execv("/bin/mkdir", argv);
	}
	if(fork()==0){
		if(fork()==0){
			char *argv[] = {"mv", "/home/nadya/sisop/shift3/quote/quote.txt", "/home/nadya/sisop/shift3/hasil", NULL};
			execv("/bin/mv", argv);
		}

		if(fork()==0){
			char *argv[] = {"mv", "/home/nadya/sisop/shift3/music/music.txt", "/home/nadya/sisop/shift3/hasil", NULL};
			execv("/bin/mv", argv);
		}
	}
	while ((wait(&status)) > 0);

	//soal d
	if(fork() == 0) {
		struct stat info;
		int r;
		r = stat("/home/nadya/sisop/shift3", &info);
		struct passwd *pw = getpwuid(info.st_uid);
		char password1[350] = "mihinomenest";
		if(pw != 0) strcat(password1, pw->pw_name);
		execl("/usr/bin/zip", "zip", "-r", "-e", "-P", password1, "/home/nadya/sisop/shift3/hasil", "hasil", "-q", NULL);
	}

	//soal e
	i=0;
	while(i<3)
	{
		err=pthread_create(&(tid3[i]),NULL,&soal_e,NULL);
		if(err!=0)
		{
			printf("\n can't create thread : [%s]",strerror(err));
		}
		else
		{
			printf("\n create thread success\n");
		}
		i++;
	}
	pthread_join(tid3[0],NULL);
	pthread_join(tid3[1],NULL);
	pthread_join(tid3[2],NULL);

	if(fork() == 0) {
		struct stat info;
		int r;
		r = stat("/home/nadya/sisop/shift3", &info);
		struct passwd *pw = getpwuid(info.st_uid);
		char password1[350] = "mihinomenest";
		if(pw != 0) strcat(password1, pw->pw_name);
		execl("/usr/bin/zip", "zip", "-r", "-e", "-P", password1, "/home/nadya/sisop/shift3/hasil",  "./no.txt", "hasil", "-q", NULL);
	}


	exit(0);
	return 0;
}
