#include<stdio.h>
#include<sys/socket.h>
#include<stdlib.h>
#include<netinet/in.h>
#include<string.h>
#include<strings.h>
#include<unistd.h>
#include<pthread.h>
#include<stdbool.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <ctype.h>
#include <netdb.h>
#define PORT 8080
//#define SVR "/home/aaliyah/sisop/modul3-soal2/Client/"

void error(char *err);
bool check_pass(char* buffer);
bool check_user(char* buffer);
bool check_login(char* buffer);
bool login_pass(char* user, char* buffer);
bool remove_char(char* str, int index);

void error(char *err){
    perror(err);
    exit(1);
}

bool check_user(char* buffer){
    FILE *fp = fopen("/home/aaliyah/sisop/modul3/users.txt", "r");
    int len = 1024;
    char temp[len];
    char copy[len];

    char* ptr;
    long long index;

    ptr = strchr(buffer, '\n');

    printf("\n\nPTR _____%s_____\nBUFFER _____%s_____\n", ptr, buffer);
    
    if(ptr != NULL){
        index = (long long) (ptr - buffer);
        strcpy(copy, buffer);
        bzero(buffer, len);
        strncpy(buffer, copy, index);
    }

    printf("BUFFER GOING IN _____%s_____\n", buffer);
    printf("SIZEOF BUFFER _____%lu_____\n", sizeof(buffer));

    while(fgets(temp, len, fp)) {
        if(temp == NULL) break;

        ptr = strchr(temp, ':');
        if(ptr != NULL){
            index = (int) (ptr - temp);
            strcpy(copy, temp);
            bzero(temp, len);
            strncpy(temp, copy, index);
        }

        printf("TEMP IS ------------%s-----------", temp);
        printf("%s strcmp %s == %d\n\n\n\n\n", buffer, temp, strcmp(buffer, temp));
        if(!strcmp(buffer, temp)) {
            fclose(fp);
            return false;
        }
    }

    fclose(fp);

    return true;
}

bool check_pass(char* buffer){
    bool has_upper = false;
    bool has_lower = false;
    bool has_numeric = false;
    int char_count = 0;
    
    for(int i = 0; i < strlen(buffer); i++){
        if(isupper(buffer[i])){
            has_upper = true;
            char_count++;
        }
        if(islower(buffer[i])) {
            has_lower = true;
            char_count++;
        }if(isdigit(buffer[i])){
            has_numeric = true;
        }
    }

    return (has_upper && has_lower && has_numeric && (char_count >= 6));
}

bool check_login(char* buffer){
    FILE *fp = fopen("/home/aaliyah/sisop/modul3/users.txt", "r");
    int len =1024;
    char temp[len];
    char copy[len];

    char* ptr;
    long long index;

    ptr = strchr(buffer, '\n');

    if(ptr != NULL){
        index = (long long) (ptr - buffer);
        strcpy(copy, buffer);
        bzero(buffer, len);
        strncpy(buffer, copy, index);
    }

    while(fgets(temp, len, fp)) {
        if(temp == NULL) break;

        ptr = strchr(temp, ':');
        if(ptr != NULL){
            index = (int) (ptr - temp);
            strcpy(copy, temp);
            bzero(temp, len);
            strncpy(temp, copy, index);
        }

        printf("TEMP IS ------------%s-----------", temp);
        printf("%s strcmp %s == %d\n\n\n\n\n", buffer, temp, strcmp(buffer, temp));
        if(!strcmp(buffer, temp)) {
            fclose(fp);
            return true;
        }
    }

    fclose(fp);

    return false;
}

bool login_pass(char* user, char* buffer){
    FILE *fp = fopen("/home/aaliyah/sisop/modul3/users.txt", "r");
    int len = 1024;
    char temp[len];
    char copy[len];

    char* ptr;
    long long index;
    
    ptr = strchr(buffer, '\n');
    if(ptr != NULL){
        index = (long long) (ptr - buffer);
        strcpy(copy, buffer);
        bzero(buffer, len);
        strncpy(buffer, copy, index);
    }

    while(fgets(temp, len, fp)) {
        if(temp == NULL) break;

        char temp_user[len];
        char* token = strtok(temp, ":");
        strcpy(temp_user, token);

        if(!strcmp(temp_user, user)) {
            // printf("FOUND USERNAME\n");
            fclose(fp);
            token = strtok(NULL, ":");
            if(token != NULL){
                // printf("TOKEN IS %s\n", token);
                remove_char(token, strlen(token) - 1);
                return !strcmp(token, buffer);
            }
        }
    }

    fclose(fp);

    return false;
}

bool remove_char(char* str, int index){
    if(index >= strlen(str)) return false;

    for(int i = 0; i < strlen(str) - index; i++){
        str[index + i] = str[index + i + 1];
        printf("%c -> %c\n", str[index + i], str[index + i + 1]);
    }

    return true;
}

bool copy_file(char* dest, char* src){
    FILE *old = fopen(src, "r");
    FILE *new = fopen(dest, "w");

    if(old == NULL || new == NULL) return false;

    int len = 1024;
    char temp[len];

    while(fgets(temp, len, old)){
        fprintf(new, "%s", temp);
    }

    fclose(old);
    fclose(new);

    return true;
}

int main(int argc, char* argv[]){
    if(argc != 2){
        fprintf(stderr, "ERROR: Port number not provided.");
        exit(1);
    }

    int socket_fd = socket(AF_INET, SOCK_STREAM, 0);
    if(socket_fd < 0) error("ERROR: Failed to open socket.\n");

    struct sockaddr_in server_addr;
    memset(&server_addr, '0', sizeof(server_addr));

    server_addr.sin_family = AF_INET;
    server_addr.sin_port = htons(PORT);

    if(inet_pton(AF_INET, "127.0.0.1", &server_addr.sin_addr)<=0) {
        printf("\nInvalid address/ Address not supported \n");
        return -1;
    }
    
    if(bind(socket_fd, (struct sockaddr *) &server_addr, sizeof(server_addr)) < 0) error("ERROR: Failed to bind socket.");
    listen(socket_fd, 5);

    while(1){
        struct sockaddr_in client_addr;
        socklen_t  client_len = sizeof(client_addr);
        
        int newsocket_fd = accept(socket_fd, (struct sockaddr *) &client_addr, &client_len);
        if(newsocket_fd < 0) error("ERROR: Accept failed.\n");

        int buffer_len = 1024;
        char buffer_in[buffer_len];
        char buffer_out[buffer_len];
        int rw_flag;

        int logged_in = 0;

        int mode = 0;
        int submode = 0;

        bzero(buffer_out, buffer_len);
        strcpy(buffer_out, "");

        printf("Initialized server with open port number %s\n", argv[1]);
        
        bool valid_user = false;
        bool valid_pass = false; 
        
        bzero(buffer_in, buffer_len);
        bzero(buffer_out, buffer_len);

        char user[buffer_len];
        char pass[buffer_len];
        char copy[buffer_len];

        char* ptr;
        int index;
        
        while(1){ 
            //WRITE MODE
            if(1){
                bzero(buffer_out, buffer_len);
                sprintf(buffer_out, "%d %d", mode, submode);
                rw_flag = write(newsocket_fd, buffer_out, buffer_len);
                if(rw_flag < 0) error("ERROR: Failed to write to file");
                bzero(buffer_out, buffer_len);

            }

            if(!mode){
                //WRITE PROMPT
                if(strlen(buffer_out) > 0) strcat(buffer_out, "\n1. login\n2. register\n3. exit\n\n");
                else strcpy(buffer_out, "1. login\n2. register\n3. exit\n\n");
                rw_flag = write(newsocket_fd, buffer_out, strlen(buffer_out));
                if(rw_flag < 0) error("ERROR: Failed to write to file");


                //READ INPUT
                bzero(buffer_in, buffer_len);
                rw_flag = read(newsocket_fd, buffer_in, buffer_len);
                if(rw_flag < 0) error("ERROR: Failed to read mode.");

                if(atoi(buffer_in) == 1){
                    mode = 1;
                }

                if(atoi(buffer_in) == 2){
                    mode = 2;               
                }

                if(atoi(buffer_in) == 3){
                    printf("\n-----END PROGRAM-----\n");
                    break;
                }
            }  
            
            
            else if(mode == 1){
                valid_user = false;
                valid_pass = false; 
                
                bzero(buffer_in, buffer_len);
                bzero(buffer_out, buffer_len);

                bzero(user, buffer_len);
                bzero(pass, buffer_len);
                bzero(copy, buffer_len);

                ptr = NULL;
                index = 0;

                submode = 0;
                //WRITE MODE
                if(1){
                    bzero(buffer_out, buffer_len);
                    sprintf(buffer_out, "%d %d", mode, submode);
                    rw_flag = write(newsocket_fd, buffer_out, buffer_len);
                    if(rw_flag < 0) error("ERROR: Failed to write to file");
                    bzero(buffer_out, buffer_len);

                }

                strcpy(buffer_out, "");

                while(!valid_user){
                    //WRITE PROMPT
                    strcat(buffer_out, "ENTER USERNAME: ");
                    rw_flag = write(newsocket_fd, buffer_out, strlen(buffer_out));
                    if(rw_flag < 0) error("ERROR: Failed to write to file");


                    //READ INPUT
                    bzero(buffer_in, buffer_len);
                    rw_flag = read(newsocket_fd, buffer_in, buffer_len);
                    if(rw_flag < 0) error("ERROR: Failed to read username.");

                    valid_user = check_login(buffer_in);
                    
                    if(!valid_user) {
                        bzero(buffer_out, buffer_len);
                        strcpy(buffer_out, "ERROR: USERNAME DOES NOT EXIST. TRY AGAIN\n");
                    }else{
                        strcpy(user, buffer_in);
                        submode = 1;
                    }
                    
                    //WRITE MODE
                    if(1){
                        bzero(buffer_out, buffer_len);
                        sprintf(buffer_out, "%d %d", mode, submode);
                        rw_flag = write(newsocket_fd, buffer_out, buffer_len);
                        if(rw_flag < 0) error("ERROR: Failed to write to file");
                        bzero(buffer_out, buffer_len);

                    }
                }
                
                bzero(buffer_out, buffer_len);

                while(!valid_pass){
                    if(!mode) break;

                    //WRITE PROMPT
                    strcat(buffer_out, "ENTER PASSWORD: ");
                    rw_flag = write(newsocket_fd, buffer_out, strlen(buffer_out));
                    if(rw_flag < 0) error("ERROR: Failed to write to file");

                    //READ INPUT
                    bzero(buffer_in, buffer_len);
                    rw_flag = read(newsocket_fd, buffer_in, buffer_len);
                    if(rw_flag < 0) error("ERROR: Failed to read password.\n");

                    valid_pass = login_pass(user, buffer_in);

                    if(!valid_pass) {
                        bzero(buffer_out, buffer_len);
                        strcpy(buffer_out, "ERROR: INCORRECT PASSWORD.\n");
                        mode = 0;
                        submode = 0;
                    }else{
                        mode = 3;
                        submode = 0;
                    }
                    //WRITE MODE
                    if(1){
                        bzero(buffer_out, buffer_len);
                        sprintf(buffer_out, "%d %d", mode, submode);
                        rw_flag = write(newsocket_fd, buffer_out, buffer_len);
                        if(rw_flag < 0) error("ERROR: Failed to write to file");
                        bzero(buffer_out, buffer_len);

                    }
                }
            }


            else if(mode == 2){
                valid_user = false;
                valid_pass = false; 
                
                bzero(buffer_in, buffer_len);
                bzero(buffer_out, buffer_len);

                bzero(user, buffer_len);
                bzero(pass, buffer_len);
                bzero(copy, buffer_len);

                ptr = NULL;
                index = 0;
                
                submode = 0;
                //WRITE MODE
                if(1){
                    bzero(buffer_out, buffer_len);
                    sprintf(buffer_out, "%d %d", mode, submode);
                    rw_flag = write(newsocket_fd, buffer_out, buffer_len);
                    if(rw_flag < 0) error("ERROR: Failed to write to file");
                    bzero(buffer_out, buffer_len);

                }

                strcpy(buffer_out, "");

                while(!valid_user){
                    strcat(buffer_out, "REGISTER USERNAME: ");
                    rw_flag = write(newsocket_fd, buffer_out, strlen(buffer_out));
                    if(rw_flag < 0) error("ERROR: Failed to write to file");


                    bzero(buffer_in, buffer_len);
                    rw_flag = read(newsocket_fd, buffer_in, buffer_len);
                    if(rw_flag < 0) error("ERROR: Failed to read username.");

                    valid_user = check_user(buffer_in);
                    
                    if(!valid_user) {
                        bzero(buffer_out, buffer_len);
                        strcpy(buffer_out, "ERROR: USERNAME ALREADY TAKEN. TRY ANOTHER\n");
                    }else{
                        strcpy(user, buffer_in);
                    }
                    
                    if(valid_user) submode = 1;
                    //WRITE MODE
                    if(1){
                        bzero(buffer_out, buffer_len);
                        sprintf(buffer_out, "%d %d", mode, submode);
                        rw_flag = write(newsocket_fd, buffer_out, buffer_len);
                        if(rw_flag < 0) error("ERROR: Failed to write to file");
                        bzero(buffer_out, buffer_len);

                    }
                }
                

                while(!valid_pass){
                    strcpy(buffer_out, "REGISTER PASSWORD: ");
                    rw_flag = write(newsocket_fd, buffer_out, strlen(buffer_out));
                    if(rw_flag < 0) error("ERROR: Failed to write to file");

                    //READ INPUT
                    bzero(buffer_in, buffer_len);
                    rw_flag = read(newsocket_fd, buffer_in, buffer_len);
                    if(rw_flag < 0) error("ERROR: Failed to read password.");

                    valid_pass = check_pass(buffer_in);

                    if(!valid_pass) {
                        bzero(buffer_out, buffer_len);
                        strcpy(buffer_out, "ERROR: INVALID PASSWORD FORMAT. MUST HAVE 6 ALPHABET, CAPITAL AND LOWER CASE, AND A NUMBER.\n");
                    }else{
                        strcpy(pass, buffer_in);
                    }
                    
                    if(valid_pass) submode = 2;
                    //WRITE MODE
                    if(1){
                        bzero(buffer_out, buffer_len);
                        sprintf(buffer_out, "%d %d", mode, submode);
                        rw_flag = write(newsocket_fd, buffer_out, buffer_len);
                        if(rw_flag < 0) error("ERROR: Failed to write to file");
                        bzero(buffer_out, buffer_len);

                    }
                }
                

                bzero(buffer_out, buffer_len);
                strcpy(buffer_out, strcat(strcat(user, ":"), pass));
                
                ptr = strchr(buffer_out, '\n');
                if(ptr != NULL){
                    index = (int) (ptr - buffer_out);
                    strcpy(copy, buffer_out);
                    bzero(buffer_out, buffer_len);
                    strncpy(buffer_out, copy, index);
                }    

                FILE *fp = fopen("/home/aaliyah/sisop/modul3/users.txt", "a");
                if(fp == NULL) error("ERROR: Failed to open users.txt file.");
                fprintf(fp, "%s\n", buffer_out);

                fclose(fp);

                mode = 0;
                submode = 0;
            }
        

            else if(mode == 3){
                //login sukses
                submode = 0;

                while(1){
                    if(1){
                        bzero(buffer_out, buffer_len);
                        sprintf(buffer_out, "%d %d", mode, submode);
                        rw_flag = write(newsocket_fd, buffer_out, buffer_len);
                        if(rw_flag < 0) error("ERROR: Failed to write to file");
                        bzero(buffer_out, buffer_len);
                    }

                    if(submode == 0){
                        //WRITE PROMPT
                        strcpy(buffer_out, "\n\n1. add\n2. see\n3. logout\n4. download <problem-title>\n5. submit <problem-title> <output-file-path>\n\n>> ");
                        rw_flag = write(newsocket_fd, buffer_out, strlen(buffer_out));
                        if(rw_flag < 0) error("ERROR: Failed to write to file");

                        //READ INPUT
                        bzero(buffer_in, buffer_len);
                        rw_flag = read(newsocket_fd, buffer_in, buffer_len);
                        if(rw_flag < 0) error("ERROR: Failed to read mode.");

                        ptr = strchr(buffer_in, '\n');
                        index = (int) (ptr - buffer_in);
                        remove_char(buffer_in, index);

                        if(atoi(buffer_in) == 1 || !strcmp(buffer_in, "add")){
                            submode = 1;
                            printf("\n-----ADD PROGRAM-----\n");
                        }

                        else if(atoi(buffer_in) == 2 || !strcmp(buffer_in, "see")){
                            submode = 2;      
                            printf("\n-----SEE PROGRAM-----\n");         
                        }

                        else if(atoi(buffer_in) == 3 || !strcmp(buffer_in, "logout")){
                            submode = 3;
                            printf("\n-----LOGOUT SUCCESS-----\n");
                        }

                        else if(atoi(buffer_in) == 4 || strstr(buffer_in, "download ")){
                            submode = 4;
                            printf("\n-----DOWNLOAD PROGRAM-----\n");
                        }

                        else if(atoi(buffer_in) == 5 || strstr(buffer_in, "submit ")){
                            submode = 5;
                            printf("\n-----SUBMIT PROGRAM-----\n");
                        }

                        printf("BUFFER IN IS ------%s------\n", buffer_in);
                        printf("ATOI OF ONE IS -----%d-----\n", atoi(buffer_in));
                    }

                    else if(submode == 1){
                        char problem_title[buffer_len];
                        char desc_path[buffer_len];
                        char input_path[buffer_len];
                        char output_path[buffer_len];

                        //WRITE PROMPT
                        bzero(buffer_out, buffer_len);
                        strcpy(buffer_out, "ENTER PROBLEM TITLE: ");
                        rw_flag = write(newsocket_fd, buffer_out, strlen(buffer_out));
                        if(rw_flag < 0) error("ERROR: Failed to write to file");

                        //READ INPUT
                        bzero(buffer_in, buffer_len);
                        rw_flag = read(newsocket_fd, buffer_in, buffer_len);
                        if(rw_flag < 0) error("ERROR: Failed to read mode.");

                        strcpy(problem_title, buffer_in);

                        //WRITE PROMPT
                        bzero(buffer_out, buffer_len);
                        strcpy(buffer_out, "ENTER DESCRIPTION FILE PATH: ");
                        rw_flag = write(newsocket_fd, buffer_out, strlen(buffer_out));
                        if(rw_flag < 0) error("ERROR: Failed to write to file");

                        //READ INPUT
                        bzero(buffer_in, buffer_len);
                        rw_flag = read(newsocket_fd, buffer_in, buffer_len);
                        if(rw_flag < 0) error("ERROR: Failed to read mode.");

                        strcpy(desc_path, buffer_in);

                        //WRITE PROMPT
                        bzero(buffer_out, buffer_len);
                        strcpy(buffer_out, "ENTER INPUT FILE PATH: ");
                        rw_flag = write(newsocket_fd, buffer_out, strlen(buffer_out));
                        if(rw_flag < 0) error("ERROR: Failed to write to file");

                        //READ INPUT
                        bzero(buffer_in, buffer_len);
                        rw_flag = read(newsocket_fd, buffer_in, buffer_len);
                        if(rw_flag < 0) error("ERROR: Failed to read mode.");

                        strcpy(input_path, buffer_in);

                        //WRITE PROMPT
                        bzero(buffer_out, buffer_len);
                        strcpy(buffer_out, "ENTER OUTPUT FILE PATH: ");
                        rw_flag = write(newsocket_fd, buffer_out, strlen(buffer_out));
                        if(rw_flag < 0) error("ERROR: Failed to write to file");

                        //READ INPUT
                        bzero(buffer_in, buffer_len);
                        rw_flag = read(newsocket_fd, buffer_in, buffer_len);
                        if(rw_flag < 0) error("ERROR: Failed to read mode.");

                        strcpy(output_path, buffer_in);
                        
                        remove_char(problem_title, strlen(problem_title) - 1);
                        remove_char(desc_path, strlen(desc_path) - 1);
                        remove_char(input_path, strlen(input_path) - 1);
                        remove_char(output_path, strlen(output_path) - 1);
                        
                        char dir_path[buffer_len];
                        char home_path[buffer_len];
                        strcpy(home_path, "/home/aaliyah/sisop/modul3/");

                        strcpy(dir_path, strcat(home_path, problem_title));

                        mkdir(dir_path, 0700);

                        bzero(buffer_out, buffer_len);
                        strcpy(buffer_out, problem_title);
                        strcat(buffer_out, "\t");
                        strcat(buffer_out, user);
                        strcat(buffer_out, "\n");

                        FILE *database_file;
                        database_file = fopen("/home/aaliyah/sisop/modul3/problems.tsv", "a+");
                        if(database_file == NULL) {
                            error("ERROR: FAILED TO OPEN DATABASE.\n");
                        }

                        fprintf(database_file, "%s", buffer_out);

                        fclose(database_file);

                        char temp[buffer_len];

                        bzero(temp, buffer_len);
                        strcpy(temp,  dir_path);
                        strcat(temp, "/description.txt");
                        copy_file(temp, desc_path);

                        bzero(temp, buffer_len);
                        strcpy(temp, dir_path);
                        strcat(temp, "/input.txt");
                        copy_file(temp, input_path);

                        bzero(temp, buffer_len);
                        strcpy(temp, dir_path);
                        strcat(temp, "/output.txt");
                        copy_file(temp, output_path);

                        submode = 0;
                    }

                    else if(submode == 2){
                        FILE* problem_database = fopen("/home/aaliyah/sisop/modul3/problems.tsv", "r");
                        int count = 0;
                        while(fgets(buffer_in, buffer_len, problem_database)) count++;
                        fclose(problem_database);

                        sprintf(buffer_out, "%d", count);
                        printf("LINE COUNT OF -----%s------\n", buffer_out);
                    
                        //WRITE PROMPTp
                        rw_flag = write(newsocket_fd, buffer_out, strlen(buffer_out));
                        if(rw_flag < 0) error("ERROR: Failed to write to file");     

                        while(strcmp(buffer_in, "FINISHED READING.")){
                            printf("%s WAITING FOR READ TO FINISH\n", buffer_in);
                            bzero(buffer_in, buffer_len);
                            rw_flag = read(newsocket_fd, buffer_in, buffer_len);
                            if(rw_flag < 0) error("ERROR: Failed to read mode.");
                        }
                        
                        bzero(buffer_in, buffer_len);

                        problem_database = fopen("/home/aaliyah/sisop/modul3/problems.tsv", "r");
                        bzero(buffer_out, buffer_len);
                        
                        char* token;

                        for(int i = 0; i < count; i++){
                            fgets(buffer_out, buffer_len, problem_database);
                            token = strtok(buffer_out, "\t");
                            strcpy(copy, token);
                            strcat(copy, " by ");
                            token = strtok(NULL, "\t");
                            strcat(copy, token);
                            strcpy(buffer_out, copy);

                            //WRITE PROMPT
                            rw_flag = write(newsocket_fd, buffer_out, strlen(buffer_out));
                            if(rw_flag < 0) error("ERROR: Failed to write to file");                        
                            bzero(buffer_out, buffer_len);

                            printf("%s", buffer_out);
                            
                            while(strcmp(buffer_in, "FINISHED READING.")){
                                printf("%s WAITING FOR READ TO FINISH\n", buffer_in);
                                bzero(buffer_in, buffer_len);
                                rw_flag = read(newsocket_fd, buffer_in, buffer_len);
                                if(rw_flag < 0) error("ERROR: Failed to read mode.");
                            }
                            
                            bzero(buffer_in, buffer_len);
                        }

                        fclose(problem_database);
                        // mode = 3;
                        submode = 0;
                    }

                    else if(submode == 3){
                        printf("LOGGING OUT\n");
                        mode = 0;
                        submode = 0;
                        bzero(buffer_out, buffer_len);
                        strcpy(buffer_out, "LOGGED OUT.\n");
                        break;
                    }

                    else if(submode == 4){
                        submode = 0;
                        ptr = strchr(buffer_in, ' ');
                        if(ptr == NULL) break;
                        strcpy(copy, ptr);
                        remove_char(copy, 0);

                        char problem_path[buffer_len];

                        strcpy(problem_path, "/home/aaliyah/sisop/modul3/");
                        strcat(problem_path, copy);
                        strcat(problem_path, "/");

                        char desc_path[buffer_len];
                        char input_path[buffer_len];

                        strcpy(desc_path, problem_path);
                        strcat(desc_path, "description.txt");

                        strcpy(input_path, problem_path);
                        strcat(input_path, "input.txt");
                        
                        FILE* desc_fp = fopen(desc_path, "r");
                        FILE* input_fp = fopen(input_path, "r");

                        int count = 0;
                        while(fgets(buffer_in, buffer_len, desc_fp)) count++;
                        fclose(desc_fp);

                        bzero(buffer_out, buffer_len);
                        sprintf(buffer_out, "%d", count);

                        //WRITE PROMPT
                        rw_flag = write(newsocket_fd, buffer_out, strlen(buffer_out));
                        if(rw_flag < 0) error("ERROR: Failed to write to file");                        
                        bzero(buffer_out, buffer_len);
                        
                        while(strcmp(buffer_in, "FINISHED READING.")){
                            bzero(buffer_in, buffer_len);
                            rw_flag = read(newsocket_fd, buffer_in, buffer_len);
                            if(rw_flag < 0) error("ERROR: Failed to read mode.");
                        }
                        
                        bzero(buffer_in, buffer_len);

                        desc_fp = fopen(desc_path, "r");

                        for(int i = 0; i < count; i++){
                            fgets(buffer_out, buffer_len, desc_fp);
                            //WRITE PROMPT
                            rw_flag = write(newsocket_fd, buffer_out, strlen(buffer_out));
                            if(rw_flag < 0) error("ERROR: Failed to write to file");                        
                            bzero(buffer_out, buffer_len);

                            while(strcmp(buffer_in, "FINISHED READING.")){
                                // printf("%s WAITING FOR READ TO FINISH\n", buffer_in);
                                bzero(buffer_in, buffer_len);
                                rw_flag = read(newsocket_fd, buffer_in, buffer_len);
                                if(rw_flag < 0) error("ERROR: Failed to read mode.");
                            }
                        
                            bzero(buffer_in, buffer_len);
                        }
                        fclose(desc_fp);

                        count = 0;
                        while(fgets(buffer_in, buffer_len, input_fp)) count++;
                        fclose(input_fp);

                        bzero(buffer_out, buffer_len);
                        sprintf(buffer_out, "%d", count);

                        //WRITE PROMPT
                        rw_flag = write(newsocket_fd, buffer_out, strlen(buffer_out));
                        if(rw_flag < 0) error("ERROR: Failed to write to file");                        
                        bzero(buffer_out, buffer_len);
                        
                        while(strcmp(buffer_in, "FINISHED READING.")){
                            bzero(buffer_in, buffer_len);
                            rw_flag = read(newsocket_fd, buffer_in, buffer_len);
                            if(rw_flag < 0) error("ERROR: Failed to read mode.");

                        }
                        
                        bzero(buffer_in, buffer_len);

                        input_fp = fopen(input_path, "r");

                        for(int i = 0; i < count; i++){
                            fgets(buffer_out, buffer_len, input_fp);
                            //WRITE PROMPT
                            rw_flag = write(newsocket_fd, buffer_out, strlen(buffer_out));
                            if(rw_flag < 0) error("ERROR: Failed to write to file");                        
                            bzero(buffer_out, buffer_len);

                            while(strcmp(buffer_in, "FINISHED READING.")){
                                bzero(buffer_in, buffer_len);
                                rw_flag = read(newsocket_fd, buffer_in, buffer_len);
                                if(rw_flag < 0) error("ERROR: Failed to read mode.");
                            }

                            bzero(buffer_in, buffer_len);
                        }
                        fclose(input_fp);
                    }

                    else if(submode == 5){
                        // printf("COMMAND IN %s\n", buffer_in);
                        char* token;
                        char problem_title[buffer_len];
                        char output_path[buffer_len];

                        token = strtok(buffer_in, " ");
                        token = strtok(NULL, " ");
                        strcpy(problem_title, token);
                        token = strtok(NULL, " ");
                        strcpy(output_path, token);
                        
                        char problem_path[buffer_len];
                        strcpy(problem_path, "/home/aaliyah/sisop/modul3/");
                        strcat(problem_path, problem_title);
                        strcat(problem_path, "/output.txt");
                        FILE *answer_fp = fopen(problem_path, "r");

                        int count = 0;
                        while(fgets(buffer_in, buffer_len, answer_fp)) count++;
                        fclose(answer_fp);

                        //READ INPUT
                        bzero(buffer_in, buffer_len);
                        rw_flag = read(newsocket_fd, buffer_in, buffer_len);
                        if(rw_flag < 0) error("ERROR: Failed to read mode.");

                        
                        //WRITE PROMPT
                        bzero(buffer_out, buffer_len);
                        sprintf(buffer_out, "%d", (atoi(buffer_in) == count));
                        rw_flag = write(newsocket_fd, buffer_out, strlen(buffer_out));
                        if(rw_flag < 0) error("ERROR: Failed to write to file");
                        
                        bool is_correct = true;

                        answer_fp = fopen(problem_path, "r");
                        if((atoi(buffer_in) == count)){
                            char temp[buffer_len];
                            for(int i = 0; i < count; i++){
                                fgets(temp, buffer_len, answer_fp);

                                //READ INPUT
                                bzero(buffer_in, buffer_len);
                                rw_flag = read(newsocket_fd, buffer_in, buffer_len);
                                if(rw_flag < 0) error("ERROR: Failed to read mode.");

                                //CONFIRM READ STATUS
                                bzero(buffer_out, buffer_len);
                                strcpy(buffer_out, "FINISHED READING.");
                                rw_flag = write(newsocket_fd, buffer_out, strlen(buffer_out));
                                if(rw_flag < 0) error("ERROR: Failed to read file.");

                                // printf("%s", buffer_out);

                                is_correct = is_correct & (!strcmp(buffer_in, temp));
                            }
                            //WRITE PROMPT
                            bzero(buffer_out, buffer_len);
                            if(is_correct) strcpy(buffer_out, "\n\nAC\n\n");
                            else strcpy(buffer_out, "\n\nWA\n\n");
                            rw_flag = write(newsocket_fd, buffer_out, strlen(buffer_out));
                            if(rw_flag < 0) error("ERROR: Failed to write to file");
                        
                        }else{
                            //WRITE PROMPT
                            bzero(buffer_out, buffer_len);
                            strcpy(buffer_out, "\n\nWA\n\n");
                            rw_flag = write(newsocket_fd, buffer_out, strlen(buffer_out));
                            if(rw_flag < 0) error("ERROR: Failed to write to file");
                        }
                        submode = 0;
                    }
                }
            }
        }

        close(newsocket_fd);
    }

    close(socket_fd);

    return 0;
}

