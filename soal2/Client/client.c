#include <stdio.h>
#include <sys/socket.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <string.h>
#include <strings.h>
#include <unistd.h>
#include <pthread.h>
#include <stdbool.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <ctype.h>
#include <netdb.h>
#define PORT 8080

void error(char *err);
bool remove_char(char* str, int index);

void error(char *err){
    perror(err);
    exit(1);
}

bool remove_char(char* str, int index){
    if(index >= strlen(str)) return false;

    for(int i = 0; i < strlen(str) - index; i++){
        str[index + i] = str[index + i + 1];
    }
    return true;
}

int main(int argc, char* argv[]){
    if(argc != 3){
        fprintf(stderr, "ERROR: Incorrect function parameter input.");
        exit(1);
    }

    int socket_fd = socket(AF_INET, SOCK_STREAM, 0);
    if(socket_fd < 0) error("ERROR: Failed to open socket.");

    struct hostent* server = gethostbyname(argv[1]);
    if(!server){
        fprintf(stderr, "ERROR: Server unavailable.");
    }
 
    struct sockaddr_in server_addr;
    memset(&server_addr, '0', sizeof(server_addr));

    server_addr.sin_family = AF_INET;
    server_addr.sin_port = htons(PORT);

    if(inet_pton(AF_INET, "127.0.0.1", &server_addr.sin_addr)<=0) {
        printf("\nInvalid address/ Address not supported \n");
        return -1;
    }

    if(connect(socket_fd, (struct sockaddr *) &server_addr, sizeof(server_addr)) < 0) error("ERROR: Failed to connect.");

    int buffer_len = 1024;
    char buffer_in[buffer_len];
    char buffer_out[buffer_len];
    int rw_flag;

    int logged_in = 0;

    printf("Initialized client on server %s with open port number %s\n", argv[1], argv[2]);

    int mode;
    int submode;

    char* token;

    while(1){
        //READ MODE & SUBMODE
        if(1){
            bzero(buffer_in, buffer_len);
            rw_flag = read(socket_fd, buffer_in, buffer_len);
            if(rw_flag < 0) error("ERROR: Failed to read to file.");
            token = strtok(buffer_in, " ");
            mode = atoi(token);
            token = strtok(NULL, " ");
            submode = atoi(token);
            bzero(buffer_in, buffer_len);

            printf("\n\n-----MODE %d // SUBMODE %d-----\n\n", mode, submode);
        }

        if(!mode){

            //READ PROMPT
            bzero(buffer_in, buffer_len);
            rw_flag = read(socket_fd, buffer_in, buffer_len);
            if(rw_flag < 0) error("ERROR: Failed to read to file.");
            printf("%s", buffer_in);

            //WRITE INPUT
            bzero(buffer_out, buffer_len);
            fgets(buffer_out, buffer_len, stdin);
            rw_flag = write(socket_fd, buffer_out, strlen(buffer_out));
            if(rw_flag < 0) error("ERROR: Failed to read file.");
            printf("\n");
            
            if(atoi(buffer_out) == 3){
                printf("-----END PROGRAM-----\n");
                break;
            }
        }


        else if(mode == 1){
            //READ MODE & SUBMODE
            if(1){
                bzero(buffer_in, buffer_len);
                rw_flag = read(socket_fd, buffer_in, buffer_len);
                if(rw_flag < 0) error("ERROR: Failed to read to file.");
                token = strtok(buffer_in, " ");
                mode = atoi(token);
                token = strtok(NULL, " ");
                submode = atoi(token);
                bzero(buffer_in, buffer_len);
            }

            while(submode == 0){
                //READ PROMPT
                bzero(buffer_in, buffer_len);
                rw_flag = read(socket_fd, buffer_in, buffer_len);
                if(rw_flag < 0) error("ERROR: Failed to read to file.");
                printf("%s", buffer_in);

                //WRITE INPUT
                bzero(buffer_out, buffer_len);
                fgets(buffer_out, buffer_len, stdin);
                rw_flag = write(socket_fd, buffer_out, strlen(buffer_out));
                if(rw_flag < 0) error("ERROR: Failed to read file.");
                printf("\n");
            
                //READ MODE & SUBMODE
                if(1){
                    bzero(buffer_in, buffer_len);
                    rw_flag = read(socket_fd, buffer_in, buffer_len);
                    if(rw_flag < 0) error("ERROR: Failed to read to file.");
                    token = strtok(buffer_in, " ");
                    mode = atoi(token);
                    token = strtok(NULL, " ");
                    submode = atoi(token);
                    bzero(buffer_in, buffer_len);
                }
            }

            bzero(buffer_in, buffer_len);

            while(submode == 1){
                if(!mode) break;

                //READ PROMPT
                bzero(buffer_in, buffer_len);
                rw_flag = read(socket_fd, buffer_in, buffer_len);
                if(rw_flag < 0) error("ERROR: Failed to read to file.");
                printf("%s", buffer_in);

                //WRITE INPUT
                bzero(buffer_out, buffer_len);
                fgets(buffer_out, buffer_len, stdin);
                rw_flag = write(socket_fd, buffer_out, strlen(buffer_out));
                if(rw_flag < 0) error("ERROR: Failed to read file.");
                printf("\n");
            
                //READ MODE & SUBMODE
                if(1){
                    bzero(buffer_in, buffer_len);
                    rw_flag = read(socket_fd, buffer_in, buffer_len);
                    if(rw_flag < 0) error("ERROR: Failed to read to file.");
                    token = strtok(buffer_in, " ");
                    mode = atoi(token);
                    token = strtok(NULL, " ");
                    submode = atoi(token);
                    bzero(buffer_in, buffer_len);
                }
            }
        }


        else if(mode == 2){
            //READ MODE & SUBMODE
            if(1){
                bzero(buffer_in, buffer_len);
                rw_flag = read(socket_fd, buffer_in, buffer_len);
                if(rw_flag < 0) error("ERROR: Failed to read to file.");
                token = strtok(buffer_in, " ");
                mode = atoi(token);
                token = strtok(NULL, " ");
                submode = atoi(token);
                bzero(buffer_in, buffer_len);
            }

            while(submode == 0){
                //READ PROMPT
                bzero(buffer_in, buffer_len);
                rw_flag = read(socket_fd, buffer_in, buffer_len);
                if(rw_flag < 0) error("ERROR: Failed to read to file.");
                printf("%s ", buffer_in);

                //WRITE INPUT
                bzero(buffer_out, buffer_len);
                fgets(buffer_out, buffer_len, stdin);
                rw_flag = write(socket_fd, buffer_out, strlen(buffer_out));
                if(rw_flag < 0) error("ERROR: Failed to read file.");
                printf("\n");
            
                //READ MODE & SUBMODE
                if(1){
                    bzero(buffer_in, buffer_len);
                    rw_flag = read(socket_fd, buffer_in, buffer_len);
                    if(rw_flag < 0) error("ERROR: Failed to read to file.");
                    token = strtok(buffer_in, " ");
                    mode = atoi(token);
                    token = strtok(NULL, " ");
                    submode = atoi(token);

                    bzero(buffer_in, buffer_len);

                }
            }

            while(submode == 1){
                //READ PROMPT
                bzero(buffer_in, buffer_len);
                rw_flag = read(socket_fd, buffer_in, buffer_len);
                if(rw_flag < 0) error("ERROR: Failed to read to file.");
                printf("%s ", buffer_in);

                //WRITE INPUT
                bzero(buffer_out, buffer_len);
                fgets(buffer_out, buffer_len, stdin);
                rw_flag = write(socket_fd, buffer_out, strlen(buffer_out));
                if(rw_flag < 0) error("ERROR: Failed to read file.");
                printf("\n");
            
                //READ MODE & SUBMODE
                if(1){
                    bzero(buffer_in, buffer_len);
                    rw_flag = read(socket_fd, buffer_in, buffer_len);
                    if(rw_flag < 0) error("ERROR: Failed to read to file.");
                    token = strtok(buffer_in, " ");
                    mode = atoi(token);
                    token = strtok(NULL, " ");
                    submode = atoi(token);

                    bzero(buffer_in, buffer_len);

                }
            }
        }
    
        else if(mode == 3){

            while(1){
                //READ MODE & SUBMODE
                if(1){
                    bzero(buffer_in, buffer_len);
                    rw_flag = read(socket_fd, buffer_in, buffer_len);
                    if(rw_flag < 0) error("ERROR: Failed to read to file.");
                    token = strtok(buffer_in, " ");
                    mode = atoi(token);
                    token = strtok(NULL, " ");
                    submode = atoi(token);
                    bzero(buffer_in, buffer_len);
                }
                
                if(submode == 0){
                    //READ PROMPT
                    bzero(buffer_in, buffer_len);
                    rw_flag = read(socket_fd, buffer_in, buffer_len);
                    if(rw_flag < 0) error("ERROR: Failed to read to file.");
                    printf("%s", buffer_in);

                    //WRITE INPUT
                    bzero(buffer_out, buffer_len);
                    fgets(buffer_out, buffer_len, stdin);
                    rw_flag = write(socket_fd, buffer_out, strlen(buffer_out));
                    if(rw_flag < 0) error("ERROR: Failed to read file.");
                    printf("\n");
                }

                else if(submode == 1){
                    //READ PROMPT
                    bzero(buffer_in, buffer_len);
                    rw_flag = read(socket_fd, buffer_in, buffer_len);
                    if(rw_flag < 0) error("ERROR: Failed to read to file.");
                    printf("%s", buffer_in);

                    //WRITE INPUT
                    bzero(buffer_out, buffer_len);
                    fgets(buffer_out, buffer_len, stdin);
                    rw_flag = write(socket_fd, buffer_out, strlen(buffer_out));
                    if(rw_flag < 0) error("ERROR: Failed to read file.");
                    printf("\n");

                    //READ PROMPT
                    bzero(buffer_in, buffer_len);
                    rw_flag = read(socket_fd, buffer_in, buffer_len);
                    if(rw_flag < 0) error("ERROR: Failed to read to file.");
                    printf("%s", buffer_in);

                    //WRITE INPUT
                    bzero(buffer_out, buffer_len);
                    fgets(buffer_out, buffer_len, stdin);
                    rw_flag = write(socket_fd, buffer_out, strlen(buffer_out));
                    if(rw_flag < 0) error("ERROR: Failed to read file.");
                    printf("\n");

                    //READ PROMPT
                    bzero(buffer_in, buffer_len);
                    rw_flag = read(socket_fd, buffer_in, buffer_len);
                    if(rw_flag < 0) error("ERROR: Failed to read to file.");
                    printf("%s", buffer_in);

                    //WRITE INPUT
                    bzero(buffer_out, buffer_len);
                    fgets(buffer_out, buffer_len, stdin);
                    rw_flag = write(socket_fd, buffer_out, strlen(buffer_out));
                    if(rw_flag < 0) error("ERROR: Failed to read file.");
                    printf("\n");

                    //READ PROMPT
                    bzero(buffer_in, buffer_len);
                    rw_flag = read(socket_fd, buffer_in, buffer_len);
                    if(rw_flag < 0) error("ERROR: Failed to read to file.");
                    printf("%s", buffer_in);

                    //WRITE INPUT
                    bzero(buffer_out, buffer_len);
                    fgets(buffer_out, buffer_len, stdin);
                    rw_flag = write(socket_fd, buffer_out, strlen(buffer_out));
                    if(rw_flag < 0) error("ERROR: Failed to read file.");
                    printf("\n");
                }
                
                else if(submode == 2){
                    //READ PROMPT
                    bzero(buffer_in, buffer_len);
                    rw_flag = read(socket_fd, buffer_in, buffer_len);
                    if(rw_flag < 0) error("ERROR: Failed to read to file.");

                    //CONFIRM READ STATUS
                    bzero(buffer_out, buffer_len);
                    strcpy(buffer_out, "FINISHED READING.");
                    rw_flag = write(socket_fd, buffer_out, strlen(buffer_out));
                    if(rw_flag < 0) error("ERROR: Failed to read file.");

                    int count = atoi(buffer_in);

                    for(int i = 0; i < count; i++){
                        
                        //READ PROMPT
                        bzero(buffer_in, buffer_len);
                        rw_flag = read(socket_fd, buffer_in, buffer_len);
                        if(rw_flag < 0) error("ERROR: Failed to read to file.");
                        printf("%s", buffer_in);

                        //CONFIRM READ STATUS
                        bzero(buffer_out, buffer_len);
                        strcpy(buffer_out, "FINISHED READING.");
                        rw_flag = write(socket_fd, buffer_out, strlen(buffer_out));
                        if(rw_flag < 0) error("ERROR: Failed to read file.");
                    
                    }
                }
                
                else if(submode == 3){break;}
                
                else if(submode == 4){
                    char* ptr;
                    char copy[buffer_len];
                    ptr = strchr(buffer_out, ' ');
                    if(ptr == NULL) break;
                    strcpy(copy, ptr);
                    remove_char(copy, 0);
                    remove_char(copy, strlen(copy) - 1);

                    char problem_path[buffer_len];

                    strcpy(problem_path, "/home/aaliyah/sisop/modul3/Client/");
                    strcat(problem_path, copy);
                    strcat(problem_path, "/");

                    mkdir(problem_path, 0700);

                    char desc_path[buffer_len];
                    char input_path[buffer_len];

                    strcpy(desc_path, problem_path);
                    strcat(desc_path, "description.txt");

                    strcpy(input_path, problem_path);
                    strcat(input_path, "input.txt");
                    
                    int count;
                    FILE* desc_fp = fopen(desc_path, "w");
                    FILE* input_fp = fopen(input_path, "w");

                    //READ PROMPT
                    bzero(buffer_in, buffer_len);
                    rw_flag = read(socket_fd, buffer_in, buffer_len);
                    if(rw_flag < 0) error("ERROR: Failed to read to file.");

                    count = atoi(buffer_in);

                    //CONFIRM READ STATUS
                    bzero(buffer_out, buffer_len);
                    strcpy(buffer_out, "FINISHED READING.");
                    rw_flag = write(socket_fd, buffer_out, strlen(buffer_out));
                    if(rw_flag < 0) error("ERROR: Failed to read file.");
                    
                    for(int i = 0; i < count; i++){
                        bzero(buffer_in, buffer_len);
                        rw_flag = read(socket_fd, buffer_in, buffer_len);
                        if(rw_flag < 0) error("ERROR: Failed to read to file.");
                        fprintf(desc_fp, "%s", buffer_in);

                        //CONFIRM READ STATUS
                        bzero(buffer_out, buffer_len);
                        strcpy(buffer_out, "FINISHED READING.");
                        rw_flag = write(socket_fd, buffer_out, strlen(buffer_out));
                        if(rw_flag < 0) error("ERROR: Failed to read file.");
                    }


                    //READ PROMPT
                    bzero(buffer_in, buffer_len);
                    rw_flag = read(socket_fd, buffer_in, buffer_len);
                    if(rw_flag < 0) error("ERROR: Failed to read to file.");

                    count = atoi(buffer_in);

                    bzero(buffer_out, buffer_len);
                    strcpy(buffer_out, "FINISHED READING.");
                    rw_flag = write(socket_fd, buffer_out, strlen(buffer_out));
                    if(rw_flag < 0) error("ERROR: Failed to read file.");
                    
                    for(int i = 0; i < count; i++){
                        bzero(buffer_in, buffer_len);
                        rw_flag = read(socket_fd, buffer_in, buffer_len);
                        if(rw_flag < 0) error("ERROR: Failed to read to file.");
                        fprintf(input_fp, "%s", buffer_in);

                        //CONFIRM READ STATUS
                        bzero(buffer_out, buffer_len);
                        strcpy(buffer_out, "FINISHED READING.");
                        rw_flag = write(socket_fd, buffer_out, strlen(buffer_out));
                        if(rw_flag < 0) error("ERROR: Failed to read file.");
                    }

                    

                    fclose(desc_fp);
                    fclose(input_fp);
                }
                
                else if(submode == 5){
                    char* token;
                    char problem_title[buffer_len];
                    char output_path[buffer_len];

                    token = strtok(buffer_out, " ");
                    token = strtok(NULL, " ");
                    strcpy(problem_title, token);
                    token = strtok(NULL, " ");
                    strcpy(output_path, token);
                    remove_char(output_path, strlen(output_path) - 1);

                    FILE *answer_fp = fopen(output_path, "r");
                    int count = 0;
                    while(fgets(buffer_in, buffer_len, answer_fp)) count++;
                    fclose(answer_fp);

                    //WRITE INPUT
                    bzero(buffer_out, buffer_len);
                    sprintf(buffer_out, "%d", count);
                    rw_flag = write(socket_fd, buffer_out, strlen(buffer_out));
                    if(rw_flag < 0) error("ERROR: Failed to read file.");
                    printf("\n");

                    //READ PROMPT
                    bzero(buffer_in, buffer_len);
                    rw_flag = read(socket_fd, buffer_in, buffer_len);
                    if(rw_flag < 0) error("ERROR: Failed to read to file.");
                    // printf("%s", buffer_in);

                    if(atoi(buffer_in)){
                        answer_fp = fopen(output_path, "r");
                        for(int i = 0; i < count; i++){
                            fgets(buffer_out, buffer_len, answer_fp);
                            
                            //WRITE INPUT
                            rw_flag = write(socket_fd, buffer_out, strlen(buffer_out));
                            if(rw_flag < 0) error("ERROR: Failed to read file.");
                            
                            while(strcmp(buffer_in, "FINISHED READING.")){
                                bzero(buffer_in, buffer_len);
                                rw_flag = read(socket_fd, buffer_in, buffer_len);
                                if(rw_flag < 0) error("ERROR: Failed to read mode.");
                            }
                            
                            
                            bzero(buffer_in, buffer_len);
                        }
                        //READ PROMPT
                        bzero(buffer_in, buffer_len);
                        rw_flag = read(socket_fd, buffer_in, buffer_len);
                        if(rw_flag < 0) error("ERROR: Failed to read to file.");
                        printf("%s", buffer_in);

                    }else{
                        //READ PROMPT
                        bzero(buffer_in, buffer_len);
                        rw_flag = read(socket_fd, buffer_in, buffer_len);
                        if(rw_flag < 0) error("ERROR: Failed to read to file.");
                        printf("%s", buffer_in);
                    }
                }
            }
        }
    }

    return 0;
}
